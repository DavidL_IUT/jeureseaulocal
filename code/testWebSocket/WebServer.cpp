#include "WebServer.hpp"


const unsigned int WebServer::serverPort = 31545;


WebServer::WebServer(){
	
	std::cout << "Creating the WebServer.." << std::endl;
	
	// Make the port listening
	if (!listener.listen(serverPort) == sf::Socket::Done){
		std::cout << "Please make free your " << serverPort << "port please!" << std::endl;
	}

	// Add the listener to the selector
	selector.add(listener);
	
	std::cout << "The WebServer is listening for connections.." << std::endl;
}


void WebServer::loop(){
	while(1){
		// Make the selector wait for data on any socket
		if (selector.wait()){
			// Test the listener
			if (selector.isReady(listener)){
				// The listener is ready: there is a pending connection
				sf::TcpSocket* client = new sf::TcpSocket;
				if (listener.accept(*client) == sf::Socket::Done){
				    add(*client);
				    send(*client);
				}
				else{
				    // Error, we won't get a new connection, delete the socket
				    delete client;
				}
			}
			else{
				// The listener socket is not ready, test all other sockets (the clients)
				for (std::list<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); ++it){
				    sf::TcpSocket& client = **it;
				    if (selector.isReady(client)){
				        // The client has sent some data, we can receive it
				        receive(client);
				    }
				}
			}
		}
	}
}

void WebServer::add(sf::TcpSocket& clientSocket){
	if (!exist(clientSocket)){
		// Add the new client to the clients list
		clients.push_back(&clientSocket);
		// Add the new client to the selector so that we will be notified when he sends something
		selector.add(clientSocket);
		std::cout << "Client " << clientSocket.getRemoteAddress() << " is connected." << std::endl;
	}
}

bool WebServer::exist(sf::TcpSocket& clientSocket){
	bool find = false;
	
	std::list<sf::TcpSocket*>::iterator it = clients.begin();
	
	while (!find && it != clients.end()){
		if ((*(*it)).getRemoteAddress() == clientSocket.getRemoteAddress()){
			find = true;
		}
		it++;
	}
	
	return find;
}

void WebServer::send(sf::TcpSocket& clientSocket){

	sf::Packet packet;
	packet << "Salut toi !!";
	if (!clientSocket.send(packet) == sf::Socket::Done){
		std::cout << "Problem when try to send some data to a client.." << std::endl;
	}
	
	std::cout << "A packet was sent to " << clientSocket.getRemoteAddress() << std::endl;
}

void WebServer::receive(sf::TcpSocket& clientSocket){
	sf::Packet packet;
	if (clientSocket.receive(packet) == sf::Socket::Done){
		std::cout << "Received a message" << std::endl;
	}
}
