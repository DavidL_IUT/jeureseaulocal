#!/bin/sh

g++ main.cpp WebServer.cpp -o WebServer -I ~/SFML/include -L ~/SFML/lib -lsfml-system -lsfml-window -lsfml-graphics -lsfml-network -std=c++11 -pedantic -pedantic-errors -Wall -Wextra -Werror -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wundef -Wzero-as-null-pointer-constant -Wuseless-cast
