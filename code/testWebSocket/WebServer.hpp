#ifndef HPP_WebServer
#define HPP_WebServer

#define MAX_CLIENT 600
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h> 
//#include <netinet/in.h>
#include <iostream>
//#include <arpa/inet.h>
#include <list>
#include <mutex>
#include <iostream>

#include <SFML/Network.hpp>
#include <SFML/System.hpp>


class WebServer{
	public:
    	WebServer();
        
        // Loop
    	void loop();
        

	private:
		sf::TcpListener listener;
        sf::TcpSocket socket;
        static const unsigned int serverPort;
        
        // A list to store the future clients
		std::list<sf::TcpSocket*> clients;
		// Create a selector
		sf::SocketSelector selector;
		
		// Tell if the socket already exist in clients list
		bool exist(sf::TcpSocket&);
		
		// Add new client
		void add(sf::TcpSocket&);
		// Communicate with client
		void send(sf::TcpSocket&);
		void receive(sf::TcpSocket&);
};

#endif

