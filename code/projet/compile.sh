#!/bin/bash

# Compile for the default target system (linux64)
cd build/linux64
cmake ../../ -DJEURESEAULOCAL_BIN_DIR=linux64 ; make
cd ../..

# Compile for win32
cd build/win32
cmake -DJEURESEAULOCAL_BIN_DIR=win32 -DCMAKE_TOOLCHAIN_FILE=toolchain-win32.cmake ../../ ; make
cd ../..
