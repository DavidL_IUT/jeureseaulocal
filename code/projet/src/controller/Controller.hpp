#ifndef HPP_CONTROLLER
#define HPP_CONTROLLER

#include <mutex>
#include <set>

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

#include "../model/Game.hpp"
#include "../view/Screen.hpp"
#include "../view/Button.hpp"

#include "NetworkClient.hpp"

class Controller{
    public:
        Controller();
    
        // Loop method
        void loop();
        
    private:        
        // Server data
        sf::Packet serverData;
        
        // Client data
        std::map<sf::IpAddress, sf::Packet> clientData;
        
        // Game, screen and network client
        Game game;
        Screen screen;
        NetworkClient networkClient;
        
        // Events
        sf::Event event;
        
        // Key bindings
        std::set<sf::Keyboard::Key> leftMovementKeySet;
        std::set<sf::Keyboard::Key> rightMovementKeySet;
        std::set<sf::Keyboard::Key> jumpKeySet;
        std::set<sf::Keyboard::Key> attackKeySet;
        std::set<sf::Keyboard::Key> heal;
        
        // Are we writing a message?
        bool writingMessage;
        sf::String message; // Used by client
        
        // Messages list
        std::list<sf::String> messages; // Used by server
        
        // The buttons
        std::list<std::shared_ptr<Button> > buttons;
        
        // Events handling methods
        void handleActiveEvents();
        void handlePassiveEvents();
        bool isKeySetPressed(std::set<sf::Keyboard::Key>&);

        //Envents Game Server :
        void startGame();
        void makeMovesIa();
        
        // Network handling methods
        void handleNetwork();
        void writeServerData();
        void writeClientData();
        void updateGameWithServerData();
        void updateGameWithClientData();
        
       static const bool ia;  // Active/desactive les IA pour test
};

sf::Packet& operator <<(sf::Packet&, const std::list<sf::String>&);
sf::Packet& operator >>(sf::Packet&, std::list<sf::String>&);

#endif
