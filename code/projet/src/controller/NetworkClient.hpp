#ifndef HPP_NetworkClient
#define HPP_NetworkClient

#include <list>
#include <mutex>
#include <iostream>

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "../view/Screen.hpp"

typedef std::pair<sf::IpAddress, unsigned short> ClientPair;

class NetworkClient{
	public:
    	NetworkClient(sf::Packet&, std::map<sf::IpAddress, sf::Packet>&);
        
        // Loop
    	void loop();
        
        // Getters
        bool getIsServer(); // MUST LOCK ISSERVERMUTEX BEFORE ACCESSING
        void setStillScreen(bool);
        bool getStillScreen();

	private:
        // Static const variables
		static const std::list<unsigned short> possiblePorts;
		static const std::string broadcastMessage;
		static const std::string broadcastMessageByServer;
		static const std::string serverBroadcastResponse;
        static const int sendingAttemptNumber;
        static const float timeToWaitForServerAnswer;
        static const float timeToWaitForClientMessage;
        static const float timeToWaitForServerData;
        static const float timeClearClientConnection;
        static const float timeClearServerConnection;
        static const float timeCheckingAnotherServer;
        
        // Socket, port, and connected clients by priority order (including ourselves)
        sf::UdpSocket socket;
    	unsigned short port; // Port on which the local socket is bound
    	std::list<ClientPair> connectedClients;
    	
        // Selector
    	sf::SocketSelector selector;
        
        // Server-specific variables
        std::map<ClientPair, sf::Clock> clientsTimers;
        sf::Clock checkingAnotherServerTimer;
        
        // Client-specific variables
        sf::IpAddress serverIp;
        unsigned short serverPort;
        sf::Clock serverTimer;
        
        // Server data
        sf::Packet& serverData;
        
        // Client data
        std::map<sf::IpAddress, sf::Packet>& clientData;
        
        // Is server?
        bool isServer;
        void setIsServer(bool);
        void pushFrontConnectedClients(sf::IpAddress, unsigned short);
        
        // Test if a given ip address is known
        bool isKnown(ClientPair);
        
        // Send a packet multiple times
        sf::Socket::Status multipleSend(sf::Packet, sf::IpAddress, unsigned short);
        
        // Broadcast-related methods
        sf::Socket::Status bindOnPort(unsigned short); // Return true if we managed to bind on the given port
        bool findAServer(); // Return true if a server was found on one of the possible ports
    	void broadcastOnPossiblePorts(std::string message = broadcastMessage); // Return true if a server was found on the given port
        bool bindOnOneOfPossiblePorts(); // Return true if we managed to bind on one of the possible ports
        bool waitForServerResponseToABroadcast(); // Return true if a server responded
        void answerClientBroadcastMessage(ClientPair); // Send a serverBroadcastReponse message to a client and add it to connected clients
        bool isPacketOneOfTheBroadcastMessage(sf::Packet&); // Test if the packet is one of the broadcast message
        bool isPacketThisString(sf::Packet&, std::string); // Test if the packet is the given string
        
        // Server-specific methods
    	void serverLoop();
        void serverReceivedPacketFromKnownClient(sf::Packet, ClientPair);
        void serverReceivedPacketFromUnknownClient(sf::Packet, ClientPair);
        void addNewConnectedClient(ClientPair);
        void clearConnexion();
        void sendDataToAllClients();
        void maybeAnotherServer(ClientPair);
        
        // Client-specific methods
    	void clientLoop();
        void sendData();
        void receiveServerData();
        void checkServer();
        
        
        // Exit
    	bool stillScreen;
};


sf::Packet& operator <<(sf::Packet& packet,const std::list<ClientPair>& connectedClients);
sf::Packet& operator >>(sf::Packet& packet, std::list<ClientPair>& connectedClients);

#endif

