#include "Controller.hpp"

#include <sstream>
#include <thread>

#include "../view/ButtonOpenShop.hpp"
#include "../view/ButtonShopButton.hpp"

#include "../algorithms.hpp"

const bool Controller::ia = false;

// Network mutexes
std::mutex serverDataMutex;
std::mutex isServerMutex;
std::mutex clientDataMutex;
std::mutex screenRunMutex;

sf::Packet& operator <<(sf::Packet& packet, const std::list<sf::String>& stringList){
    packet << static_cast<sf::Uint32>(stringList.size());
    
    for(const sf::String string : stringList){
        packet << string;
    }
    
    // Return the packet
    return packet;
}

sf::Packet& operator >>(sf::Packet& packet, std::list<sf::String>& stringList){
    // Get the string size
    sf::Uint32 stringSize;
    packet >> stringSize;
    
    // Clear the string
    stringList.clear();
    
    // Get the strings
    for(unsigned int i = 0; i < stringSize; ++i){
        sf::String string;
        packet >> string;
        stringList.push_back(string);
    }
    
    // Return the packet
    return packet;
}

Controller::Controller() : screen(buttons), networkClient(serverData, clientData){
    // Fill the key bindings
    leftMovementKeySet.insert(sf::Keyboard::Left);
    leftMovementKeySet.insert(sf::Keyboard::Q);
    leftMovementKeySet.insert(sf::Keyboard::A);
    
    rightMovementKeySet.insert(sf::Keyboard::Right);
    rightMovementKeySet.insert(sf::Keyboard::D);
    
    jumpKeySet.insert(sf::Keyboard::Up);
    jumpKeySet.insert(sf::Keyboard::Z);
    jumpKeySet.insert(sf::Keyboard::W);
    
    attackKeySet.insert(sf::Keyboard::Space);
    heal.insert(sf::Keyboard::H);
    // Add the player knight and become the king
    game.addKnight(sf::IpAddress::getLocalAddress());
    game.getKnights()[sf::IpAddress::getLocalAddress()]->setKing(true);
    
    // By default, we're not writing a message
    writingMessage = false;
    
    // Add the open shop button
    buttons.push_back(std::shared_ptr<Button>(new ButtonOpenShop(true, sf::Vector2f(-60, 5), sf::Vector2f(55, 30), std::string("Shop"), screen)));
    
    // Add the shop buttons
    buttons.push_back(std::shared_ptr<Button>(new ButtonShopButton(false, sf::Vector2f(-280, -250), sf::Vector2f(200, 30), screen, game, true, "poisson", 2)));
    
    buttons.push_back(std::shared_ptr<Button>(new ButtonShopButton(false, sf::Vector2f(-280, -210), sf::Vector2f(200, 30), screen, game, true, "staff", 0)));
    
    buttons.push_back(std::shared_ptr<Button>(new ButtonShopButton(false, sf::Vector2f(-280, -170), sf::Vector2f(200, 30), screen, game, true, "sword", 50)));

    buttons.push_back(std::shared_ptr<Button>(new ButtonShopButton(false, sf::Vector2f(-280, -130), sf::Vector2f(200, 30), screen, game, true, "hamer", 120)));
}

// controller loop
void Controller::loop(){
    std::thread networkThread(&NetworkClient::loop, &networkClient);
    
    // Create the clock
    sf::Clock clock;
    
    // While the window remains open
    while(screen.isRunning()){
        // Get the elapsed time
        float elapsedTime = clock.getElapsedTime().asSeconds();
        clock.restart();
    
        // Draw the game
        screen.draw(game);
        
        // Handle events
        handleActiveEvents();
        handlePassiveEvents();
        
        // Call the game loop method
        game.loop(elapsedTime);
        
        // Handle network
        handleNetwork();
    }
    
    networkClient.setStillScreen(false);
    
    networkThread.join();
    
    //ICI
    game.saveSave(game.getPlayerKnight());
}

void Controller::handleNetwork(){
    // Are we the server?
    bool isServerSafe = networkClient.getIsServer();
        
    // If we're the server
    if(isServerSafe){
        // IA JEREMIE
        if(ia){
            startGame();
            makeMovesIa();
        }
    
        // Update the game with client data
        updateGameWithClientData();

        // Write the game to server data
        writeServerData();
    }
    else{
        // Write the player knight to client data
        writeClientData();
    
        // Update the game with the server data
        updateGameWithServerData();
    }
}

void Controller::writeClientData(){
    // If we have a player knight
    if(game.getPlayerKnight()){
        // Lock the mutex
        clientDataMutex.lock();
        
        // If it doesn't exist yet, create the client data
        if(clientData.count(sf::IpAddress::getLocalAddress()) == 0){
            clientData[sf::IpAddress::getLocalAddress()] = sf::Packet();
        }
        
        // Empty the packet
        clientData[sf::IpAddress::getLocalAddress()].clear();
        
        // Write the data
        clientData[sf::IpAddress::getLocalAddress()] << static_cast<sf::Uint32>(game.getPlayerKnight()->getMovement());
        clientData[sf::IpAddress::getLocalAddress()] << static_cast<sf::Uint32>(game.getPlayerKnight()->getJumpingState());
        clientData[sf::IpAddress::getLocalAddress()] << static_cast<sf::Uint32>(game.getPlayerKnight()->getAttackingState());
        clientData[sf::IpAddress::getLocalAddress()] << game.getPlayerKnight()->getWeaponId();
        clientData[sf::IpAddress::getLocalAddress()] << game.getPlayerKnight()->getArmorId();
        clientData[sf::IpAddress::getLocalAddress()] << game.getPlayerKnight()->getHealMode();
        clientData[sf::IpAddress::getLocalAddress()] << game.getPlayerKnight()->getPseudo();
        
        // Possibly write the message
        if(!writingMessage && message != ""){
            clientData[sf::IpAddress::getLocalAddress()] << true;
            clientData[sf::IpAddress::getLocalAddress()] << message;
            message = "";
            game.setMessageBeingWritten(message);
        }
        else{
            clientData[sf::IpAddress::getLocalAddress()] << false;
        }
        
        // Unlock the mutex
        clientDataMutex.unlock();
    }
}

void Controller::updateGameWithClientData(){
    // Lock the mutex
    clientDataMutex.lock();
    
    // Iterate over all client data packets
    for(std::pair<const sf::IpAddress, sf::Packet>& clientDataPair : clientData){
        // If the corresponding knight does not exist yet
        if(game.getKnights().count(clientDataPair.first) == 0){
            std::cout  << "Ajout Client : " << game.getKnights().count(clientDataPair.first) << std::endl; 
            game.addKnight(clientDataPair.first);
        }
        
        // If the client data has been updated
        if(clientDataPair.second.getDataSize() > 0){
            // Get the client data as integers
            sf::Uint32 knightMovementAsInteger;
            clientDataPair.second >> knightMovementAsInteger;
            sf::Uint32 knightJumpingStateAsInteger;
            clientDataPair.second >> knightJumpingStateAsInteger;
            sf::Uint32 knightAttackStateAsInteger;
            clientDataPair.second >> knightAttackStateAsInteger;
            std::string knightWeaponId;
            clientDataPair.second >> knightWeaponId;
            std::string knightArmorId;
            clientDataPair.second >> knightArmorId;
            bool healMode;
            clientDataPair.second >> healMode;
            std::string knightPseudo;
            clientDataPair.second >> knightPseudo;
            
            // Update the game
            game.getKnights()[clientDataPair.first]->setMovement(static_cast<KnightMovement>(knightMovementAsInteger));
            game.getKnights()[clientDataPair.first]->setJumpingState(static_cast<KnightJumpingState>(knightJumpingStateAsInteger));
            game.getKnights()[clientDataPair.first]->setAttackingState(static_cast<KnightAttackingState>(knightAttackStateAsInteger));
            game.getKnights()[clientDataPair.first]->setWeaponId(knightWeaponId);
            game.getKnights()[clientDataPair.first]->setArmorId(knightArmorId);
            game.getKnights()[clientDataPair.first]->setHealMode(healMode);
            game.getKnights()[clientDataPair.first]->setPseudo(knightPseudo);
            
            // Update the game with the messages
            bool thereIsAMessage;
            clientDataPair.second >> thereIsAMessage;
            if(thereIsAMessage){
                // Get the full string
                sf::String fullString = game.getKnights()[clientDataPair.first]->getPseudo() + " : ";
                sf::String messageString;
                clientDataPair.second >> messageString;
                
                // If the message isn't empty
                if(messageString != ""){
                    fullString += messageString;
                    
                    // Add it to the game
                    game.addMessage(fullString);
                    
                    // Add it to our list
                    messages.push_back(fullString);
                }
            }
            
            // Clear the data
            clientDataPair.second.clear();
        }
    }
    
    // Iterate over all knights
    for(std::pair<const sf::IpAddress, std::shared_ptr<Knight> >& knightPair : game.getKnights()){
        // If this is not our knight
        if(knightPair.first != sf::IpAddress::getLocalAddress()){
                
        // If the corresponding client data does not exist anymore
        if(knightPair.first != "127.0.0.1"){
            if(clientData.count(knightPair.first) == 0 ){
                game.getKnights().erase(knightPair.first);
            }
          }
        }
    }
    
    // Unlock the mutex
    clientDataMutex.unlock();
}

void Controller::writeServerData(){
    // If we wrote a message
    if(!writingMessage && message != "" && game.getPlayerKnight()){
        // Get the full string
        sf::String fullString = game.getPlayerKnight()->getPseudo() + " : ";
        fullString += message;
        message = "";
        game.setMessageBeingWritten(message);
        
        // Add it to the game
        game.addMessage(fullString);
        
        // Add it to our list
        messages.push_back(fullString);
    }

    // Lock the mutex
    serverDataMutex.lock();
    
    // Empty the packet
    serverData.clear();
    
    // Write the data
    serverData << game;
    serverData << messages;
    
    // Clear the messages list
    messages.clear();
    
    // Unlock the mutex
    serverDataMutex.unlock();
}

void Controller::updateGameWithServerData(){
    // Lock the mutex
    serverDataMutex.lock();
    
    // If the server data has been updated
    if(serverData.getDataSize() > 0){
        // Update the game
        serverData >> game;
        
        // Get the messages
        std::list<sf::String> stringList;
        serverData >> stringList;
        for(sf::String& string : stringList){
            game.addMessage(string);
        }
        
        // Clear the data
        serverData.clear();
    }
    
    // Unlock the mutex
    serverDataMutex.unlock();
}

void Controller::handleActiveEvents(){    
    while(screen.pollEvent(event)){
        // Depending on the event type
        switch(event.type){
            // The user wants to close the window
            case sf::Event::Closed:
                // Close the window
                screen.closeWindow();
            break;
            case sf::Event::Resized:
                screen.adapteViews(event.size.width, event.size.height);
            break;
            // The user presses a key
            case sf::Event::KeyPressed:
                // Depending on the key code
                switch(event.key.code){
                    case sf::Keyboard::Escape:
                        // Close the window
                        screen.closeWindow();
                    break;
                    case sf::Keyboard::Return:
                        // Invert writingMessage
                        writingMessage = !writingMessage;
                        // Set to the game
                        game.setWritingMessage(writingMessage);
                        
                        // If the message is about changing the pseudo
                        if(message.find("/pseudo") == 0){
                            // Set the pseudo from what is after the command
                            if(game.getPlayerKnight()){
                                try{
                                    game.getPlayerKnight()->setPseudo(message.toAnsiString().substr(8));
                                }
                                catch(std::out_of_range& e){
                                    game.getPlayerKnight()->setPseudo("");
                                }
                            }
                            
                            // Reset the message
                            message = "";
                            game.setMessageBeingWritten(message);
                        }
                    break;
                    default: break;
                }
            break;
            // The event enters text
            case sf::Event::TextEntered:
                if(writingMessage){
                    // If the character isn't bullshit
                    if(event.text.unicode >= 32){
                        message += event.text.unicode;
                    }
                    // Else, if the character is backspace and the string isn't empty
                    else if(event.text.unicode == 8 && !message.isEmpty()){
                        message.erase(message.getSize()-1);
                    }
                    // Set the message being written
                    game.setMessageBeingWritten(message);
                }
            break;
            default: break;
        }
        
        // Send the event to the buttons
        for(std::shared_ptr<Button>& button : buttons){
            button->onEvent(screen, event);
        }
    }
}

void Controller::handlePassiveEvents(){
    // If there is a player knight and we're not writing a message
    if(game.getPlayerKnight() && !writingMessage){
        // If we press the heal key and we have enough money
        if(isKeySetPressed(heal) && game.getPlayerKnight()->getMoney() > 0){
        	game.getPlayerKnight()->setHealMode(true);
        }
        else{
        	game.getPlayerKnight()->setHealMode(false);
        }
    
        // If we press a left movement key and no right movement key and we're not in heal mode
        if(isKeySetPressed(leftMovementKeySet) && !isKeySetPressed(rightMovementKeySet) &&
           !game.getPlayerKnight()->getHealMode()){
            game.getPlayerKnight()->setMovement(KnightMovement::goingLeft);
        }
        // Else, if we press a right movement key and no left movement key and we're not in heal mode
        else if(isKeySetPressed(rightMovementKeySet) && !isKeySetPressed(leftMovementKeySet) &&
           !game.getPlayerKnight()->getHealMode()){
            game.getPlayerKnight()->setMovement(KnightMovement::goingRight);
        }
        // Else, no movement key is pressed
        else{
            game.getPlayerKnight()->setMovement(KnightMovement::notMoving);
        }
        
        // If we press a jump key and we're not in heal mode
        if(isKeySetPressed(jumpKeySet) &&
           !game.getPlayerKnight()->getHealMode()){
            game.getPlayerKnight()->setJumpingState(KnightJumpingState::jumping);
        }
        // Else, no jump key is pressed
        else{
            game.getPlayerKnight()->setJumpingState(KnightJumpingState::notJumping);
        }
        
        // If we press an attack key and we're not in heal mode and we have enough money
        if(isKeySetPressed(attackKeySet)  &&
           !game.getPlayerKnight()->getHealMode()){
            game.getPlayerKnight()->setAttackingState(KnightAttackingState::attacking);
        }
        // Else, no attack key is pressed
        else{
            game.getPlayerKnight()->setAttackingState(KnightAttackingState::notAttacking);
        }
    }
}

bool Controller::isKeySetPressed(std::set<sf::Keyboard::Key>& keySet){
    // Iterate over the keys
    for(const sf::Keyboard::Key& key : keySet){
        // If the key is pressed, return true
        if(sf::Keyboard::isKeyPressed(key)){
            return true;
        }
    }
    
    // Return false
    return false;
}

void Controller::startGame(){
    if(networkClient.getIsServer()){
        if(game.getKnights().size() < 2){
            // Si on est le serveur et que l'on est seul, on ajoute une ia
            game.addKnight("127.0.0.1");
        }
    }
}

void Controller::makeMovesIa(){
    for(std::pair<const sf::IpAddress, std::shared_ptr<Knight> >& knightPair : game.getKnights()){
        if (knightPair.first == "127.0.0.1"){
            knightPair.second->iaMove();
        }
    }
}





