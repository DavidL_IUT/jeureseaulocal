#include "NetworkClient.hpp"

const std::list<unsigned short> NetworkClient::possiblePorts = {25500, 26504, 35109, 31027};
const std::string NetworkClient::broadcastMessage = "YatilUnServer?";
const std::string NetworkClient::broadcastMessageByServer = "YatilUnServer?JensuisUn";
const std::string NetworkClient::serverBroadcastResponse = "UnServeurExisteDeja!";
const int NetworkClient::sendingAttemptNumber = 3;
const float NetworkClient::timeToWaitForServerAnswer = 1;
const float NetworkClient::timeToWaitForClientMessage = 0.0166666;
const float NetworkClient::timeToWaitForServerData = 0.0166666;
const float NetworkClient::timeClearClientConnection = 10;
const float NetworkClient::timeClearServerConnection = 1;
const float NetworkClient::timeCheckingAnotherServer = 5;

// Override operators 
sf::Packet& operator <<(sf::Packet& packet, const std::list<ClientPair>& connectedClients){
    // Put the connected clients list size
	packet << static_cast<sf::Uint32>(connectedClients.size());
    
    // Put the ip and the port
    for(const ClientPair& client : connectedClients){
        packet << client.first.toInteger() << static_cast<sf::Uint32>(client.second);
    }
    
    // Return the packet
    return packet;
}
sf::Packet& operator >>(sf::Packet& packet, std::list<ClientPair>& connectedClients){
	// Clear the connected clients list
	connectedClients.clear();
	
	// Get the list size
    sf::Uint32 listSize;
    packet >> listSize;
    
    // Variables
    sf::Uint32 ip;
    sf::Uint32 port;
    // Update the connectedClient i.e. be agree with the server
    for(unsigned int i = 0; i < listSize; i++){
        // Get the ip address and the port
        packet >> ip;
        packet >> port;
        
        // Add the connected client
        connectedClients.push_back(std::pair<sf::IpAddress, unsigned short>(sf::IpAddress(ip), static_cast<unsigned short>(port)));
    }
    
    // Return the packet
    return packet;
}

// Network mutexes
extern std::mutex serverDataMutex;
extern std::mutex isServerMutex;
extern std::mutex clientDataMutex;
extern std::mutex screenRunMutex;

NetworkClient::NetworkClient(sf::Packet& _serverData, std::map<sf::IpAddress, sf::Packet>& _clientData) :
    serverData(_serverData),
    clientData(_clientData)
{
    // Set the socket on non-blocking mode
	socket.setBlocking(false);
	setStillScreen(true);
    
    // If we manage to bind on a possible port
    if(bindOnOneOfPossiblePorts()){
        std::cout << "Socket bound on port " << port << "." << std::endl;
        // If no server was found on all possible ports
        if(!findAServer()){
            std::cout << "No server found. We become the server on port " << port << "." << std::endl;
            connectedClients.push_front(ClientPair(sf::IpAddress::getLocalAddress(), port));
            checkingAnotherServerTimer.restart(); // Initialisation of the timer which will check for other server
        }
        // Else, we found a server
        else{
            std::cout << "Found a server at " << serverIp << ":" << serverPort << "." << std::endl;
        }
    }
    // Else, we didn't found any open port
    else{
        std::cout << "No open port found :(" << std::endl;
    }
}



sf::Socket::Status NetworkClient::bindOnPort(unsigned short newPort){
    // Bind on the port and keep the status
    sf::Socket::Status status = socket.bind(newPort);
    
    // If we manage to bind on this new port, set the port
    if(status == sf::Socket::Status::Done){
        // Add the socket to the selector
        selector.add(socket);
        
        // Set the port
        port = newPort;
    }
    
    // Return the status
    return status;
}

bool NetworkClient::bindOnOneOfPossiblePorts(){
    std::cout << "Trying to bind the socket on one of the possible ports..." << std::endl;
    
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        // If we manage to bind on the possible port
        if(bindOnPort(possiblePort) == sf::Socket::Status::Done){            
            // Return true
            return true;
        }
    }
    
    // We didn't manage to bind on a port, return false
    return false;
}

void NetworkClient::setStillScreen(bool b){
	screenRunMutex.lock();
	stillScreen = b;
	screenRunMutex.unlock();
}

bool NetworkClient::getStillScreen(){
	screenRunMutex.lock();
	bool b = stillScreen;
	screenRunMutex.unlock();
	return b;
}

bool NetworkClient::findAServer(){
    std::cout << "Trying to find a server..." << std::endl;

    // Broadcast on possible ports
    broadcastOnPossiblePorts();

    // We now wait for an response from a server
    if(waitForServerResponseToABroadcast()){
		setIsServer(false);
		serverTimer.restart();
        return true;
    }
    
    // No server was found, return false
    setIsServer(true);
    return false;
}


void NetworkClient::broadcastOnPossiblePorts(std::string message){
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        std::cout << "Trying to send a broadcast on port " << possiblePort << "..." << std::endl;
        
        // Create the packet and put the broadcast message in it
        sf::Packet packet;
        packet << message;
        
        // Send the broadcast
        if(multipleSend(packet, sf::IpAddress::Broadcast, possiblePort) == sf::Socket::Done){
            std::cout << "Broadcast sent successfully." << std::endl;
        }
        else{
            std::cout << "Failed to send the broadcast on port" << possiblePort << "." << std::endl;
        }
    }
}

sf::Socket::Status NetworkClient::multipleSend(sf::Packet packet, sf::IpAddress ipAddress, unsigned short sendPort){
    // Create the status we will return in the end
    sf::Socket::Status status = sf::Socket::Status::Error;
    
    // Send
    for(int i = 0; i < sendingAttemptNumber; i++){
        if(socket.send(packet, ipAddress, sendPort) == sf::Socket::Status::Done){
            status = sf::Socket::Status::Done;
        }
    }
    
    // Return the status
    return status;
}

bool NetworkClient::waitForServerResponseToABroadcast(){
    std::cout << "Waiting for a server response to the broadcast..." << std::endl;
    
    // Create the timer
    sf::Clock timer;
    timer.restart();
    
    // Create the packet, server address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForServerAnswer) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is not us
            if(senderAddress != sf::IpAddress::getLocalAddress()){
                // Find and show the received message
                std::string receivedMessage;
                packet >> receivedMessage;
                
                // If we received a message that was correctly translated to a string (i.e. not a waitForServerResponseToABroadcastclient data packet from a client in a hurry to play before we finished waiting), we show the message in question
                if(receivedMessage != ""){
                    std::cout << "Received message " << receivedMessage << "." << std::endl;
                }
                
                // If the received message is the server broadcast response, then there is a server, we return true
                if(receivedMessage == serverBroadcastResponse){
                    // Else, if the sender of the boradcast reponse isn't known
                    if(!isKnown(ClientPair(senderAddress, senderPort))){
                        serverIp = senderAddress;
                        serverPort = senderPort;
                        return true;
                    }
                    // Else
                    else{
                        std::cout << "Concurrency problem : finding the lowest IP address..." << std::endl;
                        
                        // Find the smallest IP address between ourselves and our added connected clients
                        sf::Uint32 smallestIpNumber = sf::IpAddress::getLocalAddress().toInteger();
                        sf::IpAddress smallestIp = sf::IpAddress::getLocalAddress();
                        for(ClientPair client : connectedClients){
                            if(client.first.toInteger() < smallestIpNumber){
                                smallestIpNumber = client.first.toInteger();
                                smallestIp = client.first;
                            }
                        }
                        
                        std::cout << "Lowest IP address is " << smallestIp << "." << std::endl;
                        
                        // If we're not the smallest ip
                        if(smallestIp != sf::IpAddress::getLocalAddress()){
                            std::cout << "We're not the smallest ip, we won't be the server after all." << std::endl;
                            
                            // Return true, we won't be the server after all
                            serverIp = senderAddress;
                            serverPort = senderPort;
                            return true;
                        }
                        // Else
                        else{
                            std::cout << "We are the smallest IP, continue waiting for another server response to the broadcast..." << std::endl;
                        }
                    }
                }
                // Else, the received message is the broadcast message
                else if(receivedMessage == broadcastMessage){
                    answerClientBroadcastMessage(ClientPair(senderAddress, senderPort));
                }
            }
        }
    }

    // Return false
    return false;
}

bool NetworkClient::isKnown(ClientPair client){
	for(ClientPair& testedClient : connectedClients){
		if(client == testedClient){
			return true;
        }
	}
	return false;
}

void NetworkClient::loop(){
    while(getStillScreen()){
        isServerMutex.lock();
        bool isServerSafe = isServer;
        isServerMutex.unlock();
        
        if(isServerSafe){
            serverLoop();
        }
        else{
            clientLoop();
        }
    }
}

void NetworkClient::serverLoop(){
    // Create the packet, sender address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
    
    // Create the timer
    sf::Clock timer;
    timer.restart();
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForClientMessage) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is not us
            if(senderAddress != sf::IpAddress::getLocalAddress()){
                // If the address is known
                if(isKnown(ClientPair(senderAddress, senderPort))){
                    serverReceivedPacketFromKnownClient(packet, ClientPair(senderAddress, senderPort));
                }
                // Else
                else{
                    serverReceivedPacketFromUnknownClient(packet, ClientPair(senderAddress, senderPort));
                }
            }
        }
	}
    
    // Send data to all clients
    sendDataToAllClients();

	
	// Check if any deconnexion from client
	clearConnexion();
	
	// Broadcast on possible ports
	if (checkingAnotherServerTimer.getElapsedTime().asSeconds() > timeCheckingAnotherServer){
		checkingAnotherServerTimer.restart();
		broadcastOnPossiblePorts(broadcastMessageByServer);
	}
}

void NetworkClient::sendDataToAllClients(){
    // Lock the server data
    serverDataMutex.lock();
    
    // If the server data has been updated
    if(serverData.getDataSize() > 0){
		// Add the connected clients list and the server data to the packet to send at all clients
		sf::Packet packet;
        packet << connectedClients;
        packet.append(serverData.getData(), serverData.getDataSize());
        	
        // Send the packet to all clients
        for(ClientPair client : connectedClients){
            multipleSend(packet, client.first, client.second);
        }
        
        // Clear the data
        serverData.clear();
    }
    
    // Unlock the server data
    serverDataMutex.unlock();
}

void NetworkClient::clearConnexion(){
	// For every connected clients
	for(std::list<ClientPair>::iterator it = connectedClients.begin(); it != connectedClients.end(); it++){
        // If it is not us
        if((*it).first != sf::IpAddress::getLocalAddress()){
            // Check the timer
            if(clientsTimers[(*it)].getElapsedTime().asSeconds() > timeClearClientConnection){
                std::cout << "The client " << (*it).first << " has been disconnected." << std::endl;
                // Erase from client data
                clientDataMutex.lock();
                clientData.erase((*it).first);
                clientDataMutex.unlock();
            
				// Erase from connected clients
                it = connectedClients.erase(it);
            }
        }
  	}
}

void NetworkClient::serverReceivedPacketFromKnownClient(sf::Packet packet, ClientPair client){
    // Lock the client data mutex
    clientDataMutex.lock();
    
    // If the client data packet does not exist yet
    if(clientData.count(client.first) == 0){
        // Create the client data packet
        clientData[client.first] = sf::Packet();
    }
    
    // Clear the client data packet
    clientData[client.first].clear();
    
    // If it's not a broadcast message 
    if (!isPacketOneOfTheBroadcastMessage(packet)){
		    // Store the packet
		    clientData[client.first].append(packet.getData(), packet.getDataSize());
	  }
	  // Else if it is the broadcast message
	  else if(isPacketThisString(packet, broadcastMessage)){
		    answerClientBroadcastMessage(client);
	  }
      // Else if it is the broadcast message by server
      else if(isPacketThisString(packet, broadcastMessageByServer)){
	      std::cout << "The known client sent a broadcast server response : maybe another server?!" << std::endl;
	      
	      maybeAnotherServer(client);
      }
	  // Else if it is the broadcast server response
	  else if(isPacketThisString(packet, serverBroadcastResponse)){
	      std::cout << "The known client sent a broadcast server response : maybe another server?!" << std::endl;
	      
	      maybeAnotherServer(client);
	  }
	  
    // Unlock the client data mutex
    clientDataMutex.unlock();
    
    // Restart the client timer
    clientsTimers[client].restart();
}

void NetworkClient::serverReceivedPacketFromUnknownClient(sf::Packet receivedPacket, ClientPair client){
    std::cout << "Received packet from unknown client." << std::endl;
    
    // Find the received message
    std::string receivedMessage;
    receivedPacket >> receivedMessage;

    // If the received message is the broadcast message
    if(receivedMessage == broadcastMessage){
        std::cout << "The unknown client sent the right broadcast message." << std::endl;
        
        answerClientBroadcastMessage(client);
    }
    // Else if it is the broadcast message by server
    else if(receivedMessage == broadcastMessageByServer){
        std::cout << "The unknown client sent a broadcast server response : maybe another server?!" << std::endl;
      
        maybeAnotherServer(client);
    }
    else if(receivedMessage == serverBroadcastResponse){
        std::cout << "The unknown client sent a broadcast server response : maybe another server?!" << std::endl;

        maybeAnotherServer(client);
    }
}

void NetworkClient::maybeAnotherServer(ClientPair client){
    if (sf::IpAddress::getLocalAddress().toInteger() < client.first.toInteger()){
    		// I stay a server
    		std::cout << "Our IP address is lower : we stay the server." << std::endl;
    		
    		// Add the client
    		addNewConnectedClient(client);
    }
    else {
    		// I become a client to 'client'
    		std::cout << "Our IP address is higher : we become a client to the other server." << std::endl;
    		serverIp = client.first;
    		serverPort = client.second;
    		pushFrontConnectedClients(serverIp, serverPort);
    		serverTimer.restart();
	    	setIsServer(false);
    }
}

void NetworkClient::answerClientBroadcastMessage(ClientPair client){
    // Prepare the packet to send to the unknown client
    sf::Packet packet;
    packet << serverBroadcastResponse;
    
    std::cout << "Sending the broadcast response to the unknown client..." << std::endl;
    // If we manage to send	
    if(multipleSend(packet, client.first, client.second) == sf::Socket::Done){
        std::cout << "New client connected from " << client.first << ":" << client.second << "." << std::endl;
        
        // Add the unknown client to the list and set its timer
        addNewConnectedClient(ClientPair(client.first, client.second));
    }
    // Else
    else{
        std::cout << "Failed sending a response to the unknown client." << std::endl;
    }
}

void NetworkClient::addNewConnectedClient(ClientPair client){
    // If the client isn't known yet
    if(!isKnown(client)){
        std::cout << "Adding new connected client " << client.first << ":" << client.second << "." << std::endl;

        // Add the client
        connectedClients.push_back(client);
        clientsTimers[client] = sf::Clock();
        clientData[client.first] = sf::Packet();
    }
}

void NetworkClient::clientLoop(){
	  sendData();
    receiveServerData();
    checkServer();
}

void NetworkClient::sendData(){
    // Lock the client data mutex
    clientDataMutex.lock();
    
    // If the client data packet exists
    if(clientData.count(sf::IpAddress::getLocalAddress()) > 0){
        // If the client data packet was updated
        if(clientData[sf::IpAddress::getLocalAddress()].getDataSize() > 0){
            // Send the client data packet to the server
            multipleSend(clientData[sf::IpAddress::getLocalAddress()], serverIp, serverPort);
            
            // Clear the client data packet
            clientData[sf::IpAddress::getLocalAddress()].clear();
        }
    }
    
    // Unlock the client data mutex
    clientDataMutex.unlock();
}

void NetworkClient::receiveServerData(){
    // Create the packet, sender address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
	
    // Create the timer
    sf::Clock timer;
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForServerData) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is the server
            if(senderAddress == serverIp && senderPort == serverPort){
                // If it is not a broadcast message
                if(!isPacketOneOfTheBroadcastMessage(packet)){
					// Lock the server data
                    serverDataMutex.lock();
                    
                    // Fill the server data from the received packet
                    serverData.clear();
                    serverData.append(packet.getData(), packet.getDataSize());
                    
                    // Fill the connected clients list
                    serverData >> connectedClients;
                    
                    // Unlock the server data
                    serverDataMutex.unlock();
                    
                    // Restart the server timer
                    serverTimer.restart();
                }
                else {
                	sf::Packet testPacket(packet);
					std::string testString;
					testPacket >> testString;
	
					// Return true if the string is one of the broadcast messages
					if(testString == broadcastMessage){
						// If we're a client and we're receiving a broadcast ask
						//We ghave to send a specific msg to the asker
					}
                }
            }
        }
	}
}

void NetworkClient::checkServer(){
	if (serverTimer.getElapsedTime().asSeconds() > timeClearServerConnection){
		// The server seems to be dead
		std::cout << "The current server seems to be dead. We become the server." << std::endl;
		
		setIsServer(true);

		/*for (ClientPair client : connectedClients){
			std::cout << "Client ip " << client.first << client.second << std::endl;
		}
		
		
		// Iterator 
		std::list<ClientPair>::iterator it = connectedClients.begin();
		// Delete the server
		it = connectedClients.erase(it);
		sf::IpAddress minIp = (*it).first;
		unsigned short minPort = (*it).second;
		it++;
		// For every connectedClients
		while (it != connectedClients.end()){
			// Get the smallest IpAddress between all connected clients
			if (minIp > (*it).first){
				minIp = (*it).first;
				minPort = (*it).second;
			}
			it++;
		}
		
		serverIp = minIp;
		serverPort = minPort;
		serverTimer.restart();
		if (serverIp == sf::IpAddress::getLocalAddress()){
			// I'm he new server
			pushFrontConnectedClients(serverIp,serverPort);
			setIsServer(true);
		} else {
			setIsServer(false);
		}
		
		std::cout << " New server " << serverIp << std::endl;*/
	}
}


void NetworkClient::pushFrontConnectedClients(sf::IpAddress ip, unsigned short p){
		// Iterator 
		std::list<ClientPair>::iterator it = connectedClients.begin();
		bool deleted = false;
		// While ip not been deleted AND we're not to the end (strange if it is the case ???)
		while (!deleted && it != connectedClients.end()){
			// Get the smallest IpAddress between all connected clients
			if (ip == (*it).first){
				it = connectedClients.erase(it);
				deleted = true;
			}
			it++;
		}
		connectedClients.push_front(ClientPair(ip,p));
}


bool NetworkClient::isPacketOneOfTheBroadcastMessage(sf::Packet& packet){
    // Create the test packet and the test string
    sf::Packet testPacket(packet);
    std::string testString;
    testPacket >> testString;
    
    // Return true if the string is one of the broadcast messages
    if(testString == broadcastMessage ||
       testString == serverBroadcastResponse ||
       testString == broadcastMessageByServer){
        return true;
    }
    
    // Else return false
    return false;
}

bool NetworkClient::isPacketThisString(sf::Packet& packet, std::string str){
    // Create the test packet and the test string
    sf::Packet testPacket(packet);
    std::string testString;
    testPacket >> testString;
    
    // Return true if the string is one of the broadcast messages
    if(testString == str){
        return true;
    }
    
    // Else return false
    return false;
}

bool NetworkClient::getIsServer(){
	isServerMutex.lock();
	bool copy=isServer;
	isServerMutex.unlock();
    return copy;
}

void NetworkClient::setIsServer(bool _isServer){
    isServerMutex.lock();
    isServer = _isServer;
    isServerMutex.unlock();
}
