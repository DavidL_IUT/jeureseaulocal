#include "controller/Controller.hpp"
#include "model/Game.hpp"
#include "view/Screen.hpp"

int main(){	
	// Create the controller
	Controller controller;
	
	// Call the controller main loop
	controller.loop();
	
	return 0;
}
