#ifndef HPP_CASE
#define HPP_CASE
#include <vector>

enum class TileType{
    empty,
    ground,
    castleRock,
    backCastleRock,
    alternateCastleRock,
    vegetalCastleRock,
    vegetalGround,
    backGround,
    glass,
    spawn,
    cloud
};

class Tile{
    public:
        Tile(TileType type, bool foreground);
        
        // Getters
        TileType getTileType();
        bool getForeGround(){return foreground;}
        // Size of a tile in pixels
        static const unsigned int size;

    private:
        // Type of the Tile.
        TileType type;
        bool foreground;
};

#endif
