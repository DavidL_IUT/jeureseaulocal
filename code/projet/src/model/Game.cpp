#include "Game.hpp"

#include <fstream>
#include <iostream>
#include <set>

#include "../algorithms.hpp"

const float Game::defaultGravity = 2;
const float Game::defaultFriction = 2;
const int spawn =0;

sf::Packet& operator <<(sf::Packet& packet, const Game& game){
    packet << static_cast<sf::Uint32>(game.getKnights().size());
    
    for(const std::pair<sf::IpAddress, std::shared_ptr<Knight> >& knight : game.getKnights()){
        packet << knight.first.toInteger() << (*knight.second);
    }
    
    // Return the packet
    return packet;
}

sf::Packet& operator >>(sf::Packet& packet, Game& game){
    // Get the map size
    sf::Uint32 mapSize;
    packet >> mapSize;
    
    // Create a container to store the ip addresses present in the packet
    std::set<sf::IpAddress> ipAddresses;
    
    for(unsigned int i = 0; i < mapSize; i++){
        // Get the ip address
        sf::Uint32 ipAddressAsInteger;
        packet >> ipAddressAsInteger;
        
        // Add the ip address to the set
        ipAddresses.insert(sf::IpAddress(ipAddressAsInteger));
        
        // If the knight corresponding to the ip address doesn't exist yet
        if(game.getKnights().count(sf::IpAddress(ipAddressAsInteger)) == 0){
            game.addKnight(sf::IpAddress(ipAddressAsInteger));
        }
            
        // Position
        sf::Vector2f vector2f;
        packet >> vector2f.x;
        packet >> vector2f.y;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setPosition(vector2f);
        
        // King
        bool king;
        packet >> king;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setKing(king);
        
        // Dead
        bool dead;
        packet >> dead;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setDead(dead);
        
        // Death timer
        sf::Uint32 deathTimer;
        packet >> deathTimer;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setDeathTimer(static_cast<int>(deathTimer));
        
        // Hp
        sf::Uint32 hp;
        packet >> hp;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setHp(static_cast<int>(hp));
        
        // Movement (if the knight isn't our knight)
        sf::Uint32 movement;
        packet >> movement;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setMovement(static_cast<KnightMovement>(movement));
        }
            
        // Jumping state (if the knight isn't our knight)
        sf::Uint32 jumpingState;
        packet >> jumpingState;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setJumpingState(static_cast<KnightJumpingState>(jumpingState));
        }
        
        // Attack state (if the knight isn't our knight)
        sf::Uint32 attackingState;
        packet >> attackingState;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setAttackingState(static_cast<KnightAttackingState>(attackingState));
        }
        
        // Direction (if the knight isn't our knight)
        sf::Uint32 direction;
        packet >> direction;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setDirection(static_cast<KnightDirection>(direction));
        }
        
        // Weapon id (if the knight isn't our knight)
        std::string weaponId;
        packet >> weaponId;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setWeaponId(weaponId);
        }
        
        // Armor id (if the knight isn't our knight)
        std::string armorId;
        packet >> armorId;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setArmorId(armorId);
        }
        
        // Y speed
        float ySpeed;
        packet >> ySpeed;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setYSpeed(ySpeed);
        
        // X speed
        float xSpeed;
        packet >> xSpeed;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setXSpeed(xSpeed);
        
        // Hurt color timer
        int hurtColorTimer;
        packet >> hurtColorTimer;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setHurtColortTimer(hurtColorTimer);
        
        // Heal Mode
        bool healMode;
        packet >> healMode;
        (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setHealMode(healMode);
        
        // Pseudo
        std::string pseudo;
        packet >> pseudo;
        if(sf::IpAddress(ipAddressAsInteger) != sf::IpAddress::getLocalAddress()){
            (*game.getKnights()[sf::IpAddress(ipAddressAsInteger)]).setPseudo(pseudo);
        }
    }
    
    // Iterate over the knights
    std::map<sf::IpAddress, std::shared_ptr<Knight> >::iterator knightIterator = game.getKnights().begin();
    while(knightIterator != game.getKnights().end()){
        // If the corresponding ip address was not sent by the server
        if(ipAddresses.count((*knightIterator).first) == 0){
            std::cout << "erase knight " << std::endl;
            knightIterator = game.getKnights().erase(knightIterator);
        }
        else{
            ++knightIterator;
        }
    }
    
    // Return the packet
    return packet;
}

Game::Game(){
    // Fill the databases
	fillArmorDatabase();
	fillWeaponDatabase();
	
	// Set the game mode
	gameMode = GameMode::kingVersusPeasants;
	
	// Not writing a message at first
	writingMessage = false;
}

Game::~Game(){
    
}

// class methods
void Game::addMessage(sf::String message){
    if (messages.size() >= 10){
    messages.pop_back();
    }
    messages.push_front(message);
}

void Game::loop(float delta){
    // Iterate over the entities
    for(std::pair<sf::IpAddress, std::shared_ptr<Knight> > knight : knights){
        // Call the knight loop method
        knight.second->loop(delta);
    }
    
    // Iterate over the knights
    for(std::pair<sf::IpAddress, std::shared_ptr<Knight> > knight : knights){
        // Call the knight loop method
        knight.second->deathLoop();
    }
     redistributeCrown();
}


void Game::addKnightPosition(sf::IpAddress ipAddress, sf::Vector2f position){
    // Insert the knight in the knights container
    knights.insert(std::pair<sf::IpAddress, std::shared_ptr<Knight> >(ipAddress, std::shared_ptr<Knight>(new Knight(*this, ipAddress,position))));
    knights[ipAddress]->setWeakPtr(std::weak_ptr<Knight>(knights[ipAddress]));
    
    // charger son fichier de sauvegarde 
    if(ipAddress==sf::IpAddress::getLocalAddress())
		loadSave(knights[ipAddress]);

}
void Game::addKnight(sf::IpAddress ipAddress){
    addKnightPosition(ipAddress, getLand().getSpawnZone()[spawn]);
}

float Game::getGravity(){
    return defaultGravity;
}

float Game::getFriction(){
    return defaultFriction;
}
void Game::fillArmorDatabase(){
	std::ifstream file("./equipments/armors.csv"); 
	
	if(!file) std::cout << "Error on file opening: armors.csv" << std::endl;
    else{ 
    	// The string token used for CSV parsing
        std::string token;
        
        // Armor attributes for constructor call
        std::string id;
        std::string imageFilename;
        float blockFactor;
        float speedFactor;
        
        while(std::getline(file, token, ',')){
            id = trimString(token);
            
            std::getline(file, token, ',');
            imageFilename = trimString(token);

            std::getline(file, token, ',');
            blockFactor = std::stod(token);

            std::getline(file, token, '\n');
            speedFactor = std::stod(token);
            
            armorDatabase[id] = std::shared_ptr<Armor>(new Armor(imageFilename, blockFactor, speedFactor));
        }
    }
}

void
Game::loadSave(std::shared_ptr<Knight> knight)
{
	std::ifstream file("./save/save.dontcheat"); 
	
	if(!file) std::cout << "Error on file opening: save.dontcheat" << std::endl;
    else{
        try{
        	// The string token used for CSV parsing
            std::string token;
            
		
		    // parsing 4 lines..        
            std::getline(file, token);
            knight->setPseudo(token);
            
            std::getline(file, token);
            knight->setWeaponId(token);

		    std::getline(file, token);
            knight->setArmorId(token);
            
            std::getline(file, token);
            knight->setMoney(std::stoi(token));
            
            std::getline(file,token);
            
            
            std::string tmp;
            for (int i=0; i< std::stoi(token) ; i++){
            	std::getline(file,tmp);
            	knight->addEquipment(true,tmp);
            }
            
            std::getline(file,token);
            
            for (int i=0; i< std::stoi(token) ; i++){
            	std::getline(file,tmp);
            	knight->addEquipment(false,tmp);
            }
         }
         // We failed reading the file
         catch(std::invalid_argument e){
            knight->setPseudo("");
            knight->setWeaponId("staff");
            knight->setArmorId("nothing");
            knight->setMoney(0);
            knight->addEquipment(true,"staff");
         }
    }	





}


void
Game::saveSave(std::shared_ptr<Knight> knight)
{
	std::ofstream file("./save/save.dontcheat"); 
	
	if(!file) std::cout << "Error on file opening: save.dontcheat" << std::endl;
    else{ 
    	
		// adding 4 lines..        
        file << knight->getPseudo();
        file << std::endl;
        file << knight->getWeaponId();
		file << std::endl;
        file << knight->getArmorId();
        file << std::endl;
        file << knight->getMoney();
        file << std::endl;
        file << knight->getOwnedWeapons().size();
        file << std::endl;
        
        for(std::string str : knight->getOwnedWeapons()){
       	 file << str;
       	 file << std::endl;
        }
        file << knight->getOwnedArmors().size();
        file << std::endl;
        
        for(std::string str : knight->getOwnedArmors()){
       	 file << str;
       	 file << std::endl;
        }
        
    }	





}


void Game::fillWeaponDatabase(){
	std::ifstream file("./equipments/weapons.csv"); 
	
	if(!file) std::cout << "Error on file opening: weapons.csv" << std::endl;
    else{ 
    	// The string token used for CSV parsing
        std::string token;
        
        // Armor attributes for constructor call
        std::string id;
        std::string imageFilename;
        int damages;
        int attackTime;
        
        while(std::getline(file, token, ',')){
            id = trimString(token);
            
            std::getline(file, token, ',');
            imageFilename = trimString(token);

            std::getline(file, token, ',');
            damages = std::stoi(token);
            
            std::getline(file, token, '\n');
            attackTime = std::stoi(token);
            
            weaponDatabase[id] = std::shared_ptr<Weapon>(new Weapon(imageFilename, damages, attackTime));
        }
    }
}

std::shared_ptr<Armor> Game::getArmor(std::string armorId){
    // If the armor exists
    if(armorDatabase.count(armorId) > 0)
        return armorDatabase[armorId];
    // Else
    return armorDatabase["nothing"];
}

std::shared_ptr<Weapon> Game::getWeapon(std::string weaponId){
    // If the weapon exists
    if(weaponDatabase.count(weaponId) > 0)
        return weaponDatabase[weaponId];
    // Else
    return weaponDatabase["staff"];
}

std::shared_ptr<Knight> Game::getPlayerKnight(){
    // If there is a player knight
    if(knights.count(sf::IpAddress::getLocalAddress()) != 0){
        return knights[sf::IpAddress::getLocalAddress()];
    }
    
    // Else
    return nullptr;
}

std::list<std::shared_ptr<Knight>> Game::getKnightsInRectangle(sf::Vector2f position, sf::Vector2f size, sf::IpAddress exceptThisOne){
	std::list<std::shared_ptr<Knight> > list; 
	
    // Iterate over all knight in rectangle
	for(std::pair<sf::IpAddress, std::shared_ptr<Knight> > knight : getKnights()){
		if(knight.first != exceptThisOne && intersectRectangle(knight.second->getPosition(),static_cast<sf::Vector2f>(knight.second->getCollisionSize()),position,size)){
			list.push_front(knight.second);		
		}
	}
	return list;
}

bool Game::intersectRectangle(sf::Vector2f position1, sf::Vector2f size1 ,sf::Vector2f position2, sf::Vector2f size2){
	if(position1.x+size1.x < position2.x) return false;
	if(position1.x > position2.x+size2.x) return false;
	if(position1.y+size1.y < position2.y) return false;
	if (position1.y > position2.y+size2.y) return false; 
	return true;
}

void Game::redistributeCrown(){
    bool thereIsKing = false;
    for(std::pair<sf::IpAddress, std::shared_ptr<Knight> > knight : knights){
        // Call the knight loop method
        if(knight.second->getKing()){
         thereIsKing = true;
         break;
        }
    }
    if (!thereIsKing) {
        knights.begin()->second->setKing(true);
    }
    


}
