#include "Land.hpp"

#include <iostream> 


// Create the map with bin/map/map.txt
Land::Land()
{
	setHeight(getMapHeight());
	setWidth(getMapWidth());
	map.resize(width);
	fillTile();
}

bool Land::isObstacle(int x, int y){
    // If the given position is within the map range
    if(x >= 0 && x < static_cast<int>(width) && y >= 0 && y < static_cast<int>(height)){
        // If the tile is not ForeGround
       	if (map[static_cast<unsigned int>(x)][static_cast<unsigned int>(y)].getForeGround()){
       		return false;
       	}
       	// Depending on the tile type
        switch(map[static_cast<unsigned int>(x)][static_cast<unsigned int>(y)].getTileType()){
            case TileType::empty:
            case TileType::spawn:
            case TileType::glass:
            case TileType::backCastleRock:
            case TileType::backGround: 
                return false;
            break;
            case TileType::ground:
            case TileType::castleRock:
            case TileType::vegetalGround:
    		case TileType::vegetalCastleRock:
    		case TileType::alternateCastleRock:
			case TileType::cloud:
                return true;
            break;
        }
    }
    // Else, the position is out of the map range
    else{
        // Return true
        return true;
    }
    
    // Actually we can't reach this point because the only way would be to not return in one of the case of the switch
    // But we return in all the cases and we must enter the switch because otherwise the compiler would make a warning
    // So the proper way would be to create an unreachable exception or something like that
    // But we'll just return true
    return true;
}

std::vector<std::vector<Tile> >& Land::getMap(){
    return map;
}

unsigned int
Land::getMapHeight()
{ 
	std::ifstream file("./map/map.txt"); 
	unsigned int _height = 0;
	
	if(!file) 
	std::cout<<"Error on file opening: map.txt"<<std::endl;
	
    else{ 
    	std::string unused;
        
        while(std::getline(file, unused))
        	_height++;
    }
    
    return _height;
}

unsigned int
Land::getMapWidth()
{ 
	std::ifstream file("./map/map.txt"); 
	unsigned int _width = 0;
	unsigned int size=0;
	
	if(!file) 
	std::cout<<"Error on file opening: map.txt"<<std::endl;
    else{ 
    	std::string line = "";
        
        while(std::getline(file, line))
        {
        	if(!line.empty())
        		size = line.length();
        	if(size>_width) _width = size;
        }
    }
    
    return _width;
}

void
Land::fillTile()
{
	std::fstream f;
	f.open("./map/map.txt", std::ios::in );
	if( f.fail() ) 
	{
		std::cerr << "ouverture en lecture impossible: bin/map.txt" << std::endl;
		exit(EXIT_FAILURE);
	}
	
	unsigned int col=0; // colonne du fichier lu
	char bloc;
	for(unsigned int i=0; i<height; i++)
	{
	    f >> std::noskipws >> bloc;
		col=0;
		while(bloc!='\n')
		{	
			// bloc vide?
			if(bloc==' ') map[col].push_back(Tile(TileType::empty,false));
			
			// bloc terre
			if(bloc=='t') map[col].push_back(Tile(TileType::ground,false));
			if(bloc=='T') map[col].push_back(Tile(TileType::ground,true));
			
			
			
			// bloc  Terre Vegetal
			if(bloc=='*') map[col].push_back(Tile(TileType::backGround,false));
			if(bloc=='h') map[col].push_back(Tile(TileType::vegetalGround,false));
			if(bloc=='H') map[col].push_back(Tile(TileType::vegetalGround,true));
			
			// bloc castelRock
			if(bloc=='.') map[col].push_back(Tile(TileType::backCastleRock,false));
			if(bloc=='c') map[col].push_back(Tile(TileType::castleRock,false));
			if(bloc=='C') map[col].push_back(Tile(TileType::castleRock,true));
			// bloc castelRock Alternatif
			if(bloc=='a') map[col].push_back(Tile(TileType::alternateCastleRock,false));
			if(bloc=='A') map[col].push_back(Tile(TileType::alternateCastleRock,true));
			
			//bloc verre
			if(bloc=='V') map[col].push_back(Tile(TileType::glass,true));
			
			if(bloc=='^') map[col].push_back(Tile(TileType::cloud,false));
			
	
			// bloc végétal du castle ROCK
			if(bloc=='f') map[col].push_back(Tile(TileType::vegetalCastleRock,false));
			if(bloc=='F') map[col].push_back(Tile(TileType::vegetalCastleRock,true));
			

			if(bloc=='S'){
				 map[col].push_back(Tile(TileType::spawn,false));
				 spawnZone.push_back(sf::Vector2f(Tile::size*col,Tile::size*i));
			}	 
			
			col++;
			f >> std::noskipws >> bloc;
		}
		while(col<width) 
		{	
			// bloc vide
			map[col].push_back(Tile(TileType::empty,false));
			col++;
		}
	}
}






















