#ifndef HPP_WEAPON
#define HPP_WEAPON 

#include <string>

#include "Equipment.hpp"

class Weapon : public Equipment{
    public:
        Weapon(std::string, int, int);
        
        // getters & setters
        int getDamages(){ return damages; }
        float getAttackTime(){ return attackTime; }
        
    private:
		int damages;
		int attackTime;
};

#endif
