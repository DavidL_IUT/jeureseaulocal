#include "Tile.hpp"

const unsigned int Tile::size = 64;

Tile::Tile(TileType _type, bool _foreground){
	type = _type;
	foreground = _foreground;
} 

TileType Tile::getTileType(){
	return type;	
}
