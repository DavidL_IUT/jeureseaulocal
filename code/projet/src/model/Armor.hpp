#ifndef HPP_ARMOR
#define HPP_ARMOR 

#include <string>

#include "Equipment.hpp"

class Armor : public Equipment{
    public:
        Armor(std::string, float, float);
        
        // getters & setters
        float getBlockFactor(){ return blockFactor; }
        float getMoveSpeedFactor(){ return moveSpeedFactor; }
        
    private:
		float blockFactor;
		float moveSpeedFactor;
};

#endif
