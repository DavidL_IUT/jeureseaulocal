#ifndef HPP_LAND
#define HPP_LAND
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <SFML/Graphics.hpp>

#include "Tile.hpp"

class Land{
    public:
        Land();
        
        // Check if a tile at a given position is an obstacle
        bool isObstacle(int, int);
        
        // Getters
        std::vector<std::vector<Tile> >& getMap();
        void setWidth(unsigned int _width){ width = _width; };
        void setHeight(unsigned int _height){ height = _height; };
        void fillTile();
        unsigned int getMapHeight();
        unsigned int getMapWidth();
        std::vector<sf::Vector2f>& getSpawnZone(){ return spawnZone; };
        
    private:
        // The map
        std::vector<std::vector<Tile> > map;
        std::vector<sf::Vector2f> spawnZone;
        
        // Width and height of the map in tiles
        unsigned int width;
        unsigned int height;

};

#endif


