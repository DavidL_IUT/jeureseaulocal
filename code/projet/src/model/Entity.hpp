#ifndef HPP_ENTITY
#define HPP_ENTITY

#include <SFML/Graphics.hpp>

#include "../view/SpineAnimation.hpp"

class Game;

class Entity{
    public:
        Entity(Game& game, sf::Vector2f, std::string);
        
        // Loop method
        virtual void loop(float);
        
        // Getters
        sf::Vector2f getCloserPosition(std::vector<sf::Vector2f>&);
        sf::Vector2f getPosition() const{ return position; }
        SpineAnimation& getSpineAnimation(){ return spineAnimation; }

        // Setters
        void setPosition(sf::Vector2f);
    
    protected:
        // The game
        Game& game;
    
        // Move the entity
        void move(sf::Vector2f);
        
        // Set the animation
        void setAnimation(std::string, bool,bool);

	//Set the Skins of the Skqueletone
	virtual void setSkin(std::string skinName);
	void setPartSkin(std::string part , std::string skinName);
	void setSlot(std::string slotName);
            
    private:
        // The entity position
        sf::Vector2f position;
		
        // The spine animation
        SpineAnimation spineAnimation;
};

#endif
