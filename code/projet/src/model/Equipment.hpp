#ifndef HPP_EQUIPMENT
#define HPP_EQUIPMENT

#include <SFML/System.hpp>


class Equipment{
    public:
        Equipment(std::string);
        
        // Getters
        std::string getImageFilename(){ return imageFilename; }

    private:
        // The image filename
        std::string imageFilename;
};

#endif
