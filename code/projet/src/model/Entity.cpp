#include "Entity.hpp"

Entity::Entity(Game& _game, sf::Vector2f _position, std::string spineAnimationName) :
    game(_game),
    spineAnimation(spineAnimationName){
    setPosition(_position);
}
void Entity::setSkin(std::string skinName){ // Je fait le changement de skin dans SpineAnimation (Je trouve ça plus propre) VERIIIIF
	spineAnimation.setSkin(skinName);
}

void Entity::setPartSkin(std::string part , std::string skinName){
	spineAnimation.setPartSkin(part ,skinName);

}

void Entity::setSlot(std::string slotName){
	spineAnimation.loadSlot(slotName);

}


void Entity::move(sf::Vector2f movement){
    setPosition(position + movement);
}

void Entity::loop(float delta){
    // Call the spine animation loop
    spineAnimation.loop(delta);
}

void Entity::setPosition(sf::Vector2f _position){
    // Set the new position
    position = _position;
    
    // Update the spine animation position
    spineAnimation.setPosition(position - sf::Vector2f(0, 62)); // ça c'est vraiment très moche comme code
}

sf::Vector2f Entity::getCloserPosition(std::vector<sf::Vector2f>& tab){	
	sf::Vector2f min = tab[0];
	int dMin = sqrt(pow((position.x-tab[0].x),2)+pow((position.y-tab[0].y),2));
	for (unsigned int i =1 ; i <tab.size(); i++){
		if (sqrt(pow((position.x-tab[i].x),2)+pow((position.y-tab[i].y),2)) < dMin){
			dMin = sqrt(pow((position.x-tab[i].x),2)+pow((position.y-tab[i].y),2));
			min = tab[i];
		}
	}
	return min;
}

void Entity::setAnimation(std::string animationName, bool looping, bool flip){
    spineAnimation.setAnimation(animationName, looping, flip);
}
