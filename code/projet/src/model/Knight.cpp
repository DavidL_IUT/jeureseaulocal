#include "Knight.hpp"

#include <iostream>

#include <SFML/System.hpp>

#include "Game.hpp"

// Const to discuss
const int Knight::defaultHealthPoints = 40;
const int Knight::defaultMoveSpeed = 15;
const int Knight::defaultJumpSpeed = 20;
const int Knight::defaultDefensePoints = 10;
const int Knight::defaultAttackSpeed = 10;
const int Knight::defaultAttackPower = 1;
const int Knight::deathTime = 12; // Time of the animation or more if u want !
const int Knight::attackAnimationTime = 8; // Time of the animation or more if u want !
const int Knight::kingHorizontalPush = 40;
const int Knight::kingVerticalPush = 10;
const int Knight::hurtColorTime = 10;
const int Knight::kingMoneyTimer = 60; // Time to wait to have money for the king ! 
const int Knight::healMoneyQuantity = 1;

const float Knight::defaultKingHealthPointsBoost=2.0;  // health points
const float Knight::defaultKingMoveSpeedBoost=1.2;     // pixel move per frame
const int Knight::defaultKingAttackPowerBoost=2;   // knight's damages
const float Knight::defaultKingDefensePointsBoost=2.0; // knight's defense which reduce damages taken
const float Knight::defaultKingJumpSpeedBoost=1.35;     // pixel move per frame

const sf::Vector2<unsigned int> Knight::defaultCollisionSize = sf::Vector2<unsigned int>(90, 128);
const sf::Vector2<int> Knight::defaultAttackZone = sf::Vector2<int>(100, 190);

sf::Packet& operator << (sf::Packet& packet, const Knight& knight){
    return packet << knight.getPosition().x
                  << knight.getPosition().y
                  << knight.getKing()
                  << knight.getDead()
                  << static_cast<sf::Uint32>(knight.getDeathTimer())
                  << static_cast<sf::Uint32>(knight.getHp())
                  << static_cast<sf::Uint32>(knight.getMovement())
                  << static_cast<sf::Uint32>(knight.getJumpingState())
                  << static_cast<sf::Uint32>(knight.getAttackingState())
                  << static_cast<sf::Uint32>(knight.getDirection())
                  << knight.getWeaponId()
                  << knight.getArmorId()
                  << knight.getYSpeed()
                  << knight.getXSpeed()
                  << knight.getHurtColorTimer()
                  << knight.getHealMode()
                  << knight.getPseudo();
}

Knight::Knight(Game& _game, sf::IpAddress _ipAddress, sf::Vector2f _position) : Entity(_game, _position,"knight"), ipAddress(_ipAddress){
    // Default values
    king = false;
    hp = defaultHealthPoints;
    ySpeed = 0;
    xSpeed = 0;
    movement = KnightMovement::notMoving;
    direction = KnightDirection::right;
    jumpingState = KnightJumpingState::notJumping;
    attackingState = KnightAttackingState::notAttacking;
    pseudo = "";
    weaponId = "staff";
    addEquipment(true,"staff");
    armorId = "nothing";
    money = 0;
    dead = false;
    attackTimer = 0;
    hurtColorTimer = 0;
    kingMoneyTime = 0;
    healMode = false;
    
    setSkin("Knight");
}

void Knight::setMovement(KnightMovement knightMovement){
    // Set the knight movement
	movement = knightMovement;
	
	// Update the direction
	switch(knightMovement){
        case KnightMovement::goingLeft:
            direction = KnightDirection::left;
        break;
        case KnightMovement::goingRight:
            direction = KnightDirection::right;
        break;
	    case KnightMovement::notMoving:
	    break;
	 }
}

void Knight::iaMove(){
    std::list<std::shared_ptr<Knight> > knightsList = game.getKnightsInRectangle(getPosition() - sf::Vector2f(static_cast<sf::Vector2f>(defaultCollisionSize).x, 0), static_cast<sf::Vector2f>(defaultCollisionSize) + sf::Vector2f(static_cast<sf::Vector2f>(defaultCollisionSize).x, 0), "127.0.0.1");
         
    // If the list isn't empty
    if(!knightsList.empty()){
        if(knightsList.front()->getPosition().x+10 < getPosition().x) setMovement(KnightMovement::goingLeft);
        if(knightsList.front()->getPosition().x > getPosition().x+10) setMovement(KnightMovement::goingRight);
    }
    
    // Attack
    attack();
}

// Loop method
void Knight::loop(float delta){
    Entity::loop(delta);
    
    // Decrease the hurt color timer
    if(hurtColorTimer > 0){
        hurtColorTimer--;
    }
    if(getKing()){
    	kingMoneyTime++;
    	if (kingMoneyTime >= kingMoneyTimer){
    		kingMoneyTime = 0;
    		increaseMoney(1);
    	}
    }
    
    changeSkinEquipment("weapon", getWeaponId());
  
    if(!dead){
        // If we're in heal mode
        if(healMode){
            heal();
        }
    
        if(attackTimer < game.getWeapon(weaponId)->getAttackTime()){
            if(attackTimer == 0 && attackingState == KnightAttackingState::attacking){
                switch(direction){
                    case KnightDirection::left:
                        setAnimation("Attack", false, true);
                    break;
                    case KnightDirection::right:
                        setAnimation("Attack", false, false);
                    break;
                }
            }
            else{
                switch(jumpingState){
                    case KnightJumpingState::jumping:
                        switch(direction){
                            case KnightDirection::left:
                                setAnimation("Jump", true, true);
                            break;
                            case KnightDirection::right:
                                setAnimation("Jump", true , false);
                            break;
                        }
                    break;
                    case KnightJumpingState::notJumping:
                        switch(movement){
                            case KnightMovement::goingLeft:
                                setAnimation("Run", true, true);
                            break;
                            case KnightMovement::goingRight:
                                setAnimation("Run", true , false);
                            break;
                            case KnightMovement::notMoving:
                                switch(direction){
                                    case KnightDirection::left:
                                        setAnimation("Idle", true , true);
                                    break;
                                    case KnightDirection::right:
                                        setAnimation("Idle", true , false);
                                    break;
                                }
                            break;		      
                        }
                    break; 
                }
            }
             
            // Attack depending on the attacking state
            switch(attackingState){
                case KnightAttackingState::attacking:
                    attack();
                break;
                case KnightAttackingState::notAttacking: break;
                default: break;
            }
        }
        
        // If the timer is > to 0
        if(attackTimer > 0){
            attackTimer--;
        }
	
        // Movement and collision loop
        movementAndCollisionLoop();
	
	}
	else{
		deathTimer--;
		if(deathTimer == 0){
			resurrect();
		}	
	}
}

void Knight::deathLoop(){
    // If we're dead
    if(dead){
        // Launch the Die animation
		setAnimation("Die", true , false);
    }
    
    // If we're not dead and we have no hp
	if(!dead && hp <= 0){
        // We're dead now
        dead = true;
    
        // Set the death timer
		deathTimer = deathTime;

        // We're not king anymore!
		setKing(false);
	}
    
    // Clear the list of knight which hit us
    knightsWhichHitUs.clear();
}
void Knight::resurrect(){
    // Go to the nearest spawn point
	setPosition(getCloserPosition(game.getLand().getSpawnZone()));
  
    // Restore health points
	setHp(Knight::defaultHealthPoints);
	
  	// Init X and Y speed 
    setXSpeed(0);
	setYSpeed(0);
    
    // Reset the attack timer
    attackTimer = 0;
	
    // We're not dead anymore
    dead = false;
}


void Knight::setSkin(std::string skinName){
	Entity::setSkin(skinName);
	setSlot("Couronne");
}



void Knight::setKing(bool _king){
    // If we become the king
    if(!king && _king){
        // Become the king
        king = _king;
	setSkin("King");
    }
    // Else if we stop being the king
    if(king && !_king){
        // Stop being the king
        king = _king;
	setSkin("Knight");
        
        // Iterate over the knights which hit us
        std::shared_ptr<Knight> knightToPassTheCrown;
        for(std::weak_ptr<Knight>& knight : knightsWhichHitUs){
            // If we can access it
            if(std::shared_ptr<Knight> knightShared = knight.lock()){
                knightToPassTheCrown = knightShared;
            }
        }
        
        // Pass the crown
        if(knightToPassTheCrown){
            knightToPassTheCrown->setKing(true);
        }
        // No knight to pass the crown
        else{
            std::cout << "NO KNIGHT TO PASS THE CROWN :(" << std::endl;
        }
    }
}

void Knight::hit(std::shared_ptr<Knight>& knight){
    // Lower its hp
    knight->setHp(knight->getHp() - getAttackPower());
    
    // Add it to the list
    knight->getKnightsWhichHitUs().push_back(knightWeakPtr);
    
    // Set its hurt color timer
    knight->setHurtColortTimer(hurtColorTime);
}

void Knight::attack(){
    // If the timer is done
    if(attackTimer == 0){
        // Reset the attack timer
        attackTimer = game.getWeapon(weaponId)->getAttackTime() + attackAnimationTime;
        
        // Get knights in rectangle
        std::list<std::shared_ptr<Knight> > knightsList =
            game.getKnightsInRectangle(getPosition() + sf::Vector2f(static_cast<sf::Vector2i>(defaultCollisionSize).x/2 + (direction == KnightDirection::left? -defaultAttackZone.x : 0), 0),
                                       static_cast<sf::Vector2f>(defaultAttackZone),
                                       ipAddress);
        
        // Iterate over all the kinghts in rectangle
        for(std::shared_ptr<Knight>& knight : knightsList){
            // If the knight isn't dead
            if(!knight->getDead()){
                switch(game.getGameMode()){
                    case GameMode::freeForAll:
                        // Hit the knight
                        hit(knight);
                    break;
                    case GameMode::kingVersusPeasants:
                        // If we're not on the same team
                        if(king != knight->getKing()){
                            hit(knight);
                        }
                    break;
                }
                
                // Apply horizontal push
                switch(direction){
                    // If we're facing left
                    case KnightDirection::left:
                        // Push the knight to the left
                        knight->setXSpeed(knight->getXSpeed() - ((getKing()? kingHorizontalPush : 0) + std::min(kingHorizontalPush, std::abs(static_cast<int>(ySpeed)))));
                    break;
                    // If we're facing right
                    case KnightDirection::right:
                        // Push the knight to the right
                        knight->setXSpeed(knight->getXSpeed() + ((getKing()? kingHorizontalPush : 0) + std::min(kingHorizontalPush, std::abs(static_cast<int>(ySpeed)))));
                    break;
                }
                    
                // Apply vertical push
                knight->setYSpeed(knight->getYSpeed() - ((getKing()? kingVerticalPush : 0) + std::min(kingVerticalPush, std::abs(static_cast<int>(ySpeed)))));
            }
        }
    }
}

void Knight::movementAndCollisionLoop(){
    // Create a variable to store the possible movement
    sf::Vector2f possibleMovement = sf::Vector2f(0, 0);
    
    // Possibly increase the possible movement depending on the knight movement
    switch(movement){
        case KnightMovement::goingLeft:
            possibleMovement += sf::Vector2f(-getMoveSpeed(), 0);
        break;
        case KnightMovement::goingRight:
            possibleMovement += sf::Vector2f(getMoveSpeed(), 0);
        break;
        default: break;
    }
    
    // If there was a collision going down at last frame and we're trying to jump
    if(collisionGoingDownAtLastFrame && jumpingState == KnightJumpingState::jumping){
        // Jump
        ySpeed -= getJumpSpeed();
    }
    
    // Increase the y speed depending on the gravity
    ySpeed += game.getGravity();
    
    // Update xSpeed depending on the friction
    if(xSpeed < 0){
        xSpeed += game.getFriction();
        if(xSpeed > 0) xSpeed = 0;
    }
    else if (xSpeed > 0){
   	    xSpeed -= game.getFriction();
   	    if(xSpeed < 0) xSpeed = 0;
    }
    
    // Increase the possible movement depending on the xSpeed and ySpeed
    possibleMovement += sf::Vector2f(xSpeed, ySpeed);

    // Try the possible movement
    tryMovement(possibleMovement);
}

void Knight::tryMovement(sf::Vector2f possibleMovement){
    // By default, collisionGoingDownAtLastFrame is false
    collisionGoingDownAtLastFrame = false;
    
    // If the possible movement is along both x and y
    if(possibleMovement.x != 0 && possibleMovement.y != 0){
        // Try first the movement along x, and then along y
        tryMovement(sf::Vector2f(possibleMovement.x, 0));
        tryMovement(sf::Vector2f(0, possibleMovement.y));
    }
    // Else
    else{
        // Move the knight by the possible movement
        move(possibleMovement);
        
        // Create the minimum and maximum collision points positions we will use to restore the correct knight position if there is a collision
        sf::Vector2i minimumCollisionPoint = sf::Vector2i(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
        sf::Vector2i maximumCollisionPoint = sf::Vector2i(std::numeric_limits<int>::min(), std::numeric_limits<int>::min());
        
        // If there's a collision
        if(testCollision(minimumCollisionPoint, maximumCollisionPoint)){
            // If we were going left, stick to the maximum collision point along x
            if(possibleMovement.x < 0)
                setPosition(sf::Vector2f(maximumCollisionPoint.x, getPosition().y));
            // Else if we were going right, stick to the minimum collision point along x
            else if(possibleMovement.x > 0)
                setPosition(sf::Vector2f(minimumCollisionPoint.x - static_cast<int>(getCollisionSize().x), getPosition().y));
            // Else if we were going up, stick to the maximum collision point along y
            else if(possibleMovement.y < 0)
                setPosition(sf::Vector2f(getPosition().x, maximumCollisionPoint.y));
            // Else if we were going down, stick to the minimum collision point along y
            else if(possibleMovement.y > 0){
                setPosition(sf::Vector2f(getPosition().x, minimumCollisionPoint.y - static_cast<int>(getCollisionSize().y)));
                
                // Also set that there was a collision
                collisionGoingDownAtLastFrame = true;
            }
            
            // If the collision was along y, reset the y speed
            if(possibleMovement.y != 0){
                ySpeed = 0;
            }
            // If the collision was along x, reset the x speed
            if(possibleMovement.x != 0){
                xSpeed = 0;
            }
        }
    }
}

bool Knight::testCollision(sf::Vector2i& minimumCollisionPoint, sf::Vector2i& maximumCollisionPoint){
    // Create a boolean to store if there was a collision or not
    bool collisionBoolean = false;
    
    // Iterate over points at Tile::size pixels interval along the knight bounding box and check if the tile there is an obstacle
    float x = getPosition().x;
    bool iteratingAlongX = true;
    while(iteratingAlongX){
        float y = getPosition().y;
        bool iteratingAlongY = true;
        while(iteratingAlongY){
            // Calculate the coordinates of the tile under the current point of the knight bounding box
            int tileX = static_cast<int>(std::floor(x / Tile::size));
            int tileY = static_cast<int>(std::floor(y / Tile::size));
            
            // If we're not on an edge case (which happens when the right or down border of the bounding box aligns with the tiles)
            if((x != getPosition().x + getCollisionSize().x || x != tileX * static_cast<int>(Tile::size)) &&
               (y != getPosition().y + getCollisionSize().y || y != tileY * static_cast<int>(Tile::size))){
                // If the tile at this position is an obstacle, return true
                if(game.getLand().isObstacle(tileX, tileY)){
                    // Set the collision boolean to true
                    collisionBoolean = true;
                    
                    // Update the minimum and maximum collision points
                    minimumCollisionPoint.x = std::min(minimumCollisionPoint.x, tileX * static_cast<int>(Tile::size));
                    minimumCollisionPoint.y = std::min(minimumCollisionPoint.y, tileY * static_cast<int>(Tile::size));
                    maximumCollisionPoint.x = std::max(maximumCollisionPoint.x, (tileX + 1) * static_cast<int>(Tile::size));
                    maximumCollisionPoint.y = std::max(maximumCollisionPoint.y, (tileY + 1) * static_cast<int>(Tile::size));
                }
            }
            // If we're on the bottom of the bounding box, we can stop iterating along y
            if(y == getPosition().y + getCollisionSize().y)
                iteratingAlongY = false;
            // Else, we didn't reach the right yet, add Tile::size to y
            else{
                y += Tile::size;
                // If we went over the bottom, set y to the bottom of the bouding box
                if(y > getPosition().y + getCollisionSize().y)
                    y = getPosition().y + getCollisionSize().y;
            }
        }
        // If we're on the right of the bounding box, we can stop iterating along x
        if(x == getPosition().x + getCollisionSize().x)
            iteratingAlongX = false;
        // Else, we didn't reach the right yet, add Tile::size to x
        else{
            x += Tile::size;
            // If we went over the right, set x to the right of the bouding box
            if(x > getPosition().x + getCollisionSize().x)
                x = getPosition().x + getCollisionSize().x;
        }
    }
    
    // Return the collision boolean
    return collisionBoolean;
}

// Get the knight specs & stuff
void
Knight::increaseMoney(int amountToAdd){
	money += amountToAdd;
}

void
Knight::decreaseMoney(int amountToDecrease){
	money -= amountToDecrease;
	if (money<0) resetMoney();
}

void
Knight::setMoney(int _money){
	money = _money;
}

void
Knight::resetMoney(){
	money = 0;
}

int Knight::getAttackPower() const{ 
	if(getKing())
	{
		int boostedStat = defaultAttackPower*defaultKingAttackPowerBoost;
		return boostedStat * game.getWeapon(weaponId)->getDamages();
	}
    return defaultAttackPower * game.getWeapon(weaponId)->getDamages();
}
        
int Knight::getDefensePoints() const{
	if(getKing())
	{
		int boostedStat = static_cast<int> (defaultDefensePoints*defaultKingDefensePointsBoost);
		return boostedStat * game.getArmor(armorId)->getBlockFactor();
	}	
    return defaultDefensePoints * game.getArmor(armorId)->getBlockFactor();
}

int Knight::getMoveSpeed() const{
	if(getKing())
	{
		int boostedStat = static_cast<int> (defaultMoveSpeed*defaultKingMoveSpeedBoost);
		return boostedStat * game.getArmor(armorId)->getMoveSpeedFactor();
	}
    return defaultMoveSpeed * game.getArmor(armorId)->getMoveSpeedFactor();
}

int Knight::getJumpSpeed() const{ 
	if(getKing())
	{
		return defaultJumpSpeed*defaultKingJumpSpeedBoost;
	}
    return defaultJumpSpeed;
}

sf::Vector2<unsigned int> Knight::getCollisionSize(){
    return defaultCollisionSize;
}

void Knight::heal(){
	decreaseMoney(healMoneyQuantity);
	setHp(getHp()+1);
}


void Knight::changeSkinEquipment(std::string part, std::string skinName){
	setPartSkin(part,skinName);
}

void Knight::addEquipment(bool weapon ,std::string name){
 	// If it's weapon
	if (weapon){
		getOwnedWeapons().emplace(name);
	}
	// Else it's an armor 	
	else {
		getOwnedArmors().emplace(name);
	}

}



