#ifndef HPP_GAME
#define HPP_GAME

#include <map>
#include <memory>
#include <unordered_set>
#include <list>
#include <SFML/Network.hpp>

#include "Armor.hpp"
#include "Entity.hpp"
#include "Knight.hpp"
#include "Land.hpp"
#include "Weapon.hpp"

enum class GameMode{
    freeForAll,
    kingVersusPeasants
};

class Game{
    public:
        Game();
        ~Game();
        
        // Loop method
        void loop(float);
       
        // Database-related methods
    	void fillArmorDatabase();
    	void fillWeaponDatabase();

        // Knights-related methods
        void addKnightPosition(sf::IpAddress,sf::Vector2f); // Add a knight corresponding to a given ip address
        void addKnight(sf::IpAddress); // Add a knight corresponding to a given ip address
        void setPlayerKnight(sf::IpAddress); // Set the player knight as a knight corresponding to a given ip address
        
        // Player knight control related methods
        void setPlayerKnightMovement(KnightMovement);
        void setPlayerKnightJumpingState(KnightJumpingState);
        
        // Get the current gravity
        float getGravity();
        float getFriction();
        
        // Get equipment from the database
        std::shared_ptr<Armor> getArmor(std::string armorId);
        std::shared_ptr<Weapon> getWeapon(std::string weaponId);
        
        // Get knights in rectangle
        std::list<std::shared_ptr<Knight> > getKnightsInRectangle(sf::Vector2f position, sf::Vector2f size, sf::IpAddress);
        
        // Add a message
        void addMessage(sf::String);
        
        // Getters
        Land& getLand(){ return land; };
        std::shared_ptr<Knight> getPlayerKnight();
        std::map<sf::IpAddress, std::shared_ptr<Knight> >& getKnights(){ return knights; }
        const std::map<sf::IpAddress, std::shared_ptr<Knight> >& getKnights() const{ return knights; }
        const std::list<sf::String> getMessages() const{ return messages; }
        GameMode getGameMode(){ return gameMode; }
        sf::String getMessageBeingWritten() const{ return messageBeingWritten; }
        bool getWritingMessage() const{ return writingMessage; }
        
        // Setters
        void setMessageBeingWritten(sf::String _messageBeingWritten){ messageBeingWritten = _messageBeingWritten; }
        void setWritingMessage(bool _writingMessage){ writingMessage = _writingMessage; }
        
        // Equipment and caractere save
        void saveSave(std::shared_ptr<Knight> knight);
        
    private:
    	//Game mode
    	GameMode gameMode;
    	
        // The land
        Land land;
        
        // The knights
        std::map<sf::IpAddress, std::shared_ptr<Knight> > knights;
        
        // Equipment databases
        std::map<std::string, std::shared_ptr<Armor> > armorDatabase;
        std::map<std::string, std::shared_ptr<Weapon> > weaponDatabase;
        void loadSave(std::shared_ptr<Knight> knight);
        
        // Messages list
        std::list<sf::String> messages;
        sf::String messageBeingWritten;
        bool writingMessage;
        
        // The default gravity
        static const float defaultGravity;
        static const float defaultFriction;
        
        //Interesct beetwen 2 rectangles
        bool intersectRectangle(sf::Vector2f position1, sf::Vector2f size1, sf::Vector2f position2, sf::Vector2f size2); 

        void redistributeCrown();
};

sf::Packet& operator <<(sf::Packet& packet, const Game& game);
sf::Packet& operator >>(sf::Packet& packet, Game& game);

sf::Packet& operator <<(sf::Packet& packet, const std::map<sf::IpAddress, std::shared_ptr<Knight> >& knights);
sf::Packet& operator >>(sf::Packet& packet, std::map<sf::IpAddress, std::shared_ptr<Knight> >& knights);

#endif
