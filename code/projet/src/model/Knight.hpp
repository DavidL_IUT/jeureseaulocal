#ifndef HPP_KNIGHT
#define HPP_KNIGHT

#include <memory>
#include <list>
#include <set>

#include <SFML/Network.hpp>

#include "Entity.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"

enum class KnightMovement{
    notMoving,
    goingLeft,
    goingRight
};

enum class KnightDirection{
    left,
    right
};

enum class KnightJumpingState{
    notJumping,
    jumping
};

enum class KnightAttackingState{
    notAttacking,
    attacking
};

class Knight : public Entity{
    public:
        Knight(Game&, sf::IpAddress, sf::Vector2f position);
        
        // Loop method
        void loop(float);
        void deathLoop();
        
        // Equipment-related methods
        void unequipArmor();
        void unequipWeapon();
        void changeSkinEquipment(std::string part, std::string skinName);
        std::set<std::string>& getOwnedArmors(){return ownedArmors;}
        std::set<std::string>& getOwnedWeapons(){return ownedWeapons;}
        void addEquipment(bool weapon ,std::string name);

        
        // Calculate the knight specs, depending on its equipment
        int getAttackPower() const;
        int getDefensePoints() const;
        int getAttackSpeed() const;
        int getMoveSpeed() const;
        int getJumpSpeed() const;
        
        // Knight's money related methods
        int getMoney() const{ return money; }
        void increaseMoney(int amountToAdd);
        void decreaseMoney(int amountToDecrease);
        void setMoney(int _money);
        void resetMoney();
        
        void heal();
        
        // Calculate the knight collision size
        sf::Vector2<unsigned int> getCollisionSize();
        
        // Getters
        bool getKing() const{ return king; }
        bool getDead() const{ return dead; }
        int getDeathTimer() const{ return deathTimer; }
        int getHp() const{ return hp; }
        KnightMovement getMovement() const{ return movement; }
        KnightDirection getDirection() const{ return direction; }
        KnightJumpingState getJumpingState() const{ return jumpingState; }
        KnightAttackingState getAttackingState() const{ return attackingState; }
        float getYSpeed() const{ return ySpeed; }
        float getXSpeed() const{ return xSpeed; }
        std::string getWeaponId() const{ return weaponId; }
        std::string getArmorId() const{ return armorId; }
        sf::IpAddress getIpAdress(){return ipAddress;}
        std::list<std::weak_ptr<Knight> >& getKnightsWhichHitUs(){ return knightsWhichHitUs; }
        int getHurtColorTimer() const{ return hurtColorTimer; }
        bool getHealMode() const{ return healMode; }
        std::string getPseudo() const{ return (pseudo != ""? pseudo : sf::IpAddress::getLocalAddress().toString()); }
        
        // Setters
        void setSkin(std::string skinName);
        void setKing(bool);
        void setDead(bool _dead){ dead = _dead; }
        void setHealMode(bool _mode){ healMode= _mode;}
        void setDeathTimer(int _deathTimer){ deathTimer = _deathTimer; }
        void setHp(int _hp){ hp = _hp; }
        void setMovement(KnightMovement);
        void setDirection(KnightDirection _direction){ direction = _direction; }
        void setJumpingState(KnightJumpingState knightJumpingState){ jumpingState = knightJumpingState; }
        void setAttackingState(KnightAttackingState knightAttackingState){ attackingState = knightAttackingState; }
        void setYSpeed(float _ySpeed){ ySpeed = _ySpeed; }
        void setXSpeed(float _xSpeed){ xSpeed = _xSpeed; }
        void setArmorId(std::string _armorId){ armorId = _armorId; }
        void setWeaponId(std::string _weaponId){ weaponId = _weaponId; }
        void setWeakPtr(std::weak_ptr<Knight> _knightWeakPtr){ knightWeakPtr = _knightWeakPtr; }
        void setHurtColortTimer(int _hurtColorTimer){ hurtColorTimer = _hurtColorTimer; }
        void setPseudo(std::string _pseudo){ pseudo = _pseudo; }
        
        // King related methods
        void iaMove();

    private:
        // The knight ip & pseudo
        sf::IpAddress ipAddress;
        std::string pseudo;
        
        // The knight weak ptr
        std::weak_ptr<Knight> knightWeakPtr;
    
        // Is the Knight a king?
        bool king;
        
		// Is the knight dead?
        bool dead;
        int deathTimer; 
        
        // Attack timer
        int attackTimer;
        
        // Health points
        int hp;
        
        // amout of money
        int money;
        
        // Movement, direction, jumping state, attack state and ySpeed
        KnightMovement movement;
        KnightDirection direction;
        KnightJumpingState jumpingState;
        KnightAttackingState attackingState;
        float ySpeed;
        float xSpeed;
        
        // Hurt color timer
        int hurtColorTimer;
        
        // Curent time waited by the king 
        int kingMoneyTime;
        
        //Heal mode
        
        bool healMode;
        
        
        // Last knights which hit us
        std::list<std::weak_ptr<Knight> > knightsWhichHitUs;
        
        // Equipment
        std::string armorId;
        std::string weaponId;
        //Own Equipement
        std::set<std::string> ownedArmors;
        std::set<std::string> ownedWeapons;
        
        // Was there a collision going down at last frame? (used to know if we can jump)
        bool collisionGoingDownAtLastFrame;
        
        // Default specs
        static const int defaultHealthPoints;  // health points
        static const int defaultMoveSpeed;     // pixel move per frame
        static const int defaultJumpSpeed;     // initial pixel move per frame
        static const int defaultAttackSpeed;   // number of attacks per X frames
        static const int defaultAttackPower;   // knight's damages
        static const int defaultDefensePoints; // knight's defense which reduce damages taken
        static const int deathTime;            // time of the death animation
        static const int attackAnimationTime;  // attack animation duration
        static const int kingHorizontalPush;
        static const int kingVerticalPush;
        static const int hurtColorTime;
        static const int kingMoneyTimer;
        static const int healMoneyQuantity;
        
        
    	// Default King specs
		static const float defaultKingHealthPointsBoost;  // health points
		static const float defaultKingMoveSpeedBoost;     // pixel move per frame
		static const int defaultKingAttackPowerBoost;   // knight's damages
		static const float defaultKingDefensePointsBoost; // knight's defense which reduce damages taken
		static const float defaultKingJumpSpeedBoost;
        
        // Default collision size and attack zone
        static const sf::Vector2<unsigned int> defaultCollisionSize;
        static const sf::Vector2<int> defaultAttackZone;
        
        // Internal loop methods
        void movementAndCollisionLoop();
        void attack();
        void resurrect();
        void hit(std::shared_ptr<Knight>&);
        
        // Method used to try a movement (it will test the collisions before moving)
        void tryMovement(sf::Vector2f);
        
        // Method used to know if the knight currently collides with something
        bool testCollision(sf::Vector2i&, sf::Vector2i&);
};

sf::Packet& operator <<(sf::Packet& packet, const Knight& knight);
sf::Packet& operator >>(sf::Packet& packet, Knight& knight);

#endif
