#include "Weapon.hpp" 

Weapon::Weapon(std::string _imageFilename, int _damages, int _attackTime) : Equipment(_imageFilename){
    this->damages = _damages;
    this->attackTime = _attackTime;
}
