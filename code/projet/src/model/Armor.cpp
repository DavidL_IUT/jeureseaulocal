#include "Armor.hpp" 

Armor::Armor(std::string _imageFilename, float _blockFactor, float _moveSpeedFactor) : Equipment(_imageFilename){
    this->blockFactor = _blockFactor;
    this->moveSpeedFactor = _moveSpeedFactor;
}
