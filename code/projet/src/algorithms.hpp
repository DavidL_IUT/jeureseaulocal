// trim from start
static inline std::string &ltrimString(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrimString(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trimString(std::string &s) {
        return ltrimString(rtrimString(s));
}

// get a username from a string
static inline std::string getUsernameFromIp(sf::IpAddress ipAddress){
    return ipAddress.toString();
}

