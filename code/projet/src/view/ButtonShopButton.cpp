#include "ButtonShopButton.hpp"

#include "Screen.hpp"

#include "../model/Game.hpp"

ButtonShopButton::ButtonShopButton(bool _aboveMenuBar, sf::Vector2f _position, sf::Vector2f _size, Screen& _screen, Game& _game, bool _weapon, std::string _equipmentId, int _price) :
    Button(_aboveMenuBar, _position, _size, ""),
    screen(_screen),
    game(_game),
    weapon(_weapon),
    equipmentId(_equipmentId),
    price(_price){
    
}

void ButtonShopButton::performAction(){
    // If there is a player knight
    if(game.getPlayerKnight()){
        // If the equipment wasn't bought
        if((weapon && game.getPlayerKnight()->getOwnedWeapons().count(equipmentId) == 0) ||
           (!weapon && game.getPlayerKnight()->getOwnedArmors().count(equipmentId) == 0)){
           // If we have enough money
           if(game.getPlayerKnight()->getMoney() >= price){
               // Buy the equipment
               game.getPlayerKnight()->setMoney(game.getPlayerKnight()->getMoney() - price);
               
               // Set it
               if(weapon) game.getPlayerKnight()->setWeaponId(equipmentId);
               else game.getPlayerKnight()->setArmorId(equipmentId);
                
               // Add the equipment
               game.getPlayerKnight()->addEquipment(weapon, equipmentId);
           }
        }
        // Else
        else{
            // Set it
            if(weapon) game.getPlayerKnight()->setWeaponId(equipmentId);
            else game.getPlayerKnight()->setArmorId(equipmentId);
            
            // Add the equipment
            game.getPlayerKnight()->addEquipment(weapon, equipmentId);
        }
    }
}

sf::Vector2f ButtonShopButton::getPosition() const{
    return sf::Vector2f(screen.getWindow().getView().getSize().x, 0) + Button::getPosition() + sf::Vector2f(0, screen.getShopHeight());
}

std::string ButtonShopButton::getText() const{
    // Get the string corresponding to the equipment
    std::string equipmentString = "Not found";
    if(equipmentId == "staff") equipmentString = "the staff";
    else if(equipmentId == "sword") equipmentString = "the sword";
    else if(equipmentId == "hamer") equipmentString = "the hammer";
    else if(equipmentId == "poisson") equipmentString = "the fish";

    // If there is a player knight
    if(game.getPlayerKnight()){
        // If the equipment was bought
        if((weapon && game.getPlayerKnight()->getOwnedWeapons().count(equipmentId) != 0) ||
           (!weapon && game.getPlayerKnight()->getOwnedArmors().count(equipmentId) != 0)){
            return std::string("Equip ") + equipmentString;
        }
        // Else
        else{
            return std::string("Buy ") + equipmentString + " for " + std::to_string(price) + "$";
        }
    }
    
    // Else, no player knight
    return "";
}

