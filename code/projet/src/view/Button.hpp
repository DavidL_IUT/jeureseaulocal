#ifndef HPP_BUTTON
#define HPP_BUTTON

#include <SFML/Graphics.hpp>

class Screen;

class Button{
  public:
    Button(bool, sf::Vector2f, sf::Vector2f, std::string);
    
    // Callback called when an event is issued
    void onEvent(Screen&, sf::Event);
    
    // Method called when the button action should be performed
    virtual void performAction() = 0;
    
    // Getters
    virtual sf::Vector2f getPosition() const{ return position; }
    virtual sf::Vector2f getSize() const{ return size; }
    virtual std::string getText() const{ return text; }
    bool getPressed() const{ return pressed; }
    bool getAboveMenuBar() const{ return aboveMenuBar; }
    
  private:
    // Should the button be drawn above the menu bar?
    bool aboveMenuBar;
  
    // Position and size
    sf::Vector2f position;
    sf::Vector2f size;
    
    // Text
    std::string text;
    
    // Is the button pressed?
    bool pressed;
    
    // Is the mouse inside the button?
    bool isMouseInsideButton(Screen&);
};

#endif
