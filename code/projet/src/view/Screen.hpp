#ifndef HPP_SCREEN
#define HPP_SCREEN

#include <memory>
#include <list>

#include <SFML/Graphics.hpp>

#include "ImageManager.hpp"
#include "FontManager.hpp"

class Entity;
class Game;
class Land;
class Knight;
class Button;

class Screen{
    public:
        Screen(std::list<std::shared_ptr<Button> >&);
        ~Screen();
        
        // Check if the screen is running
        bool isRunning() const;
        
        // Drawing
        void draw(Game&);
        
        // Poll event method, called by the controller
        bool pollEvent(sf::Event&);
        
        // Close the window
        void closeWindow();
        void adapteViews(unsigned int width, unsigned int height);
        
        // Open or close shop method, called by buttons
        void openOrCloseShop();
        
        // Getters
        unsigned int getShopHeight() const{ return shopHeight; }
        sf::RenderWindow& getWindow(){ return window; }
    
    private:
        // Window
        sf::RenderWindow window;
        
        // The view
        sf::View view;
        sf::View interfaceView;
        
        // The image manager
        ImageManager imageManager;
        
        // The font manager
        FontManager fontManager;
        
        // The buttons
        std::list<std::shared_ptr<Button> >& buttons;
        
        // Open or close shop variables
        bool shopOpened;
        unsigned int shopHeight;
        
        // General drawing methods
        void prepareDrawing();
        void drawGame(Game&);
        void endDrawing();
        
        // Specific drawing methods
        void drawLand(Land&);
        void drawBackLand(Land&);
        void drawLandForeGround(Land&);
        void drawEntities(Game&);
        void drawKnight(Knight&);
        void drawKnightEquipment(Knight&);
        void centerView(Game&);  
        void drawCarac(Game&);
        void drawChat(Game&);
        void drawMenuBar();
        void drawShop(Game&);
        void drawButtons(bool);
       
        
        // Sprite-related methods
        sf::Sprite createSprite(std::string, sf::Vector2f,float width =1,float height =1);
        sf::Sprite createFlipSprite(std::string, sf::Vector2f);
        
        // Constants
        const static std::string noImageFilename;
        const static int sizeMenuBar;
        const static unsigned int shopMaxHeight;
        const static unsigned int shopPanelSpeed;
};

#endif
