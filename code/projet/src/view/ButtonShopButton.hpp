#ifndef HPP_BUTTONSHOPBUTTON
#define HPP_BUTTONSHOPBUTTON

#include "Button.hpp"

class Game;
class Screen;

class ButtonShopButton : public Button{
    public:
        ButtonShopButton(bool, sf::Vector2f, sf::Vector2f, Screen&, Game&, bool, std::string, int);
    
        // Perform action
        void performAction();
        
        // Get the button position
        virtual sf::Vector2f getPosition() const;
        virtual std::string getText() const;
    
    private:
        // The screen and the game
        Screen& screen;
        Game& game;
        
        // What are we selling and what is the id
        bool weapon;
        std::string equipmentId;
        
        // The equipment price
        int price;
};

#endif
