#include "ButtonOpenShop.hpp"

#include "Screen.hpp"

ButtonOpenShop::ButtonOpenShop(bool _aboveMenuBar, sf::Vector2f _position, sf::Vector2f _size, std::string _text, Screen& _screen) :
    Button(_aboveMenuBar, _position, _size, _text),
    screen(_screen){
    
}

void ButtonOpenShop::performAction(){
    screen.openOrCloseShop();
}

sf::Vector2f ButtonOpenShop::getPosition() const{
    return sf::Vector2f(screen.getWindow().getView().getSize().x, 0) + Button::getPosition();
}
