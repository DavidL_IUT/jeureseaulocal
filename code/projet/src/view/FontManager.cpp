#include "FontManager.hpp"

FontManager::FontManager(){
	

}

sf::Font& FontManager::filenameToFont(std::string h){
    // Add the folder name
    h = "./polices/" + h + ".ttf";
    
    // If the texture isn't loaded yet, load it
	if(map.count(h) == 0){
		FontManager::addFont(h);
    }
    
    // Return the font
    return map[h];
}

void FontManager::addFont(std::string h){
    // Create the font
	map[h] = sf::Font();
    
    // Try to load it
	if(!map[h].loadFromFile(h)){
		std::cout << "Error when loading font " << h << "." << std::endl;
	}
	std::cout << h + " correctly loaded !" << std::endl;
}
