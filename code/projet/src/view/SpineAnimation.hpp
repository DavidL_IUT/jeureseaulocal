#ifndef HPP_SPINEANIMATION
#define HPP_SPINEANIMATION

#include <string>
#include <iostream>
#include "../spine/spine-sfml.h"

class SpineAnimation{
    public:
        SpineAnimation(std::string);
        ~SpineAnimation();
        
        // Loop method
        void loop(float);
        
        // Set the animation
        void setAnimation(std::string, bool,bool);
        
        // Set the color factor
        void setColorFactor(sf::Color);
        
        // Set the skeleton position
        void setPosition(sf::Vector2f);

		//Set the Skins of the Skqueletone
		void setSkin(std::string skinName);
		//Set Part of Skins (for exemple , change only a weapons !)
		void setPartSkin(std::string part, std::string skinName);
        void loadSlot(std::string slotName);
        // Getters
        spine::SkeletonDrawable* getDrawable(){ return skeletonDrawable; }
        SkeletonData* getSkeletonData(){return skeletonData;}
        
    private:
        // Animation name
        std::string name;
        
        // Spine-related variables
        Atlas* atlas;
        SkeletonData* skeletonData;
        spine::SkeletonDrawable* skeletonDrawable;
};

#endif
