#include "Screen.hpp"

#include "Button.hpp"

#include "../model/Game.hpp"

const std::string Screen::noImageFilename = "NULL";
const int Screen::sizeMenuBar = 40;
const unsigned int Screen::shopMaxHeight = 300;
const unsigned int Screen::shopPanelSpeed = 40;

Screen::Screen(std::list<std::shared_ptr<Button> >& _buttons) :
    buttons(_buttons){
    // Create the window
    window.create(sf::VideoMode(1024,
                                768),
                                "Jeu réseau local",
                                sf::Style::Default);
                                
    // Create the view
    view = sf::View(sf::FloatRect(0, 0, 1024, 768));
    interfaceView = sf::View(sf::FloatRect(0, 0, 1024, 768));
    
    // Set the right view
    window.setView(view);
    
    // Adapt views
    adapteViews(1024, 768);
    
    // Set various window parameters
    window.setFramerateLimit(30);
    window.setKeyRepeatEnabled(false);
    
    // At first, the shop isn't opened and the shop height is 0
    shopOpened = false;
    shopHeight = 0;
}

Screen::~Screen(){ 	
    window.close();
}

void Screen::openOrCloseShop(){
    // Invert the shopOpened boolean
    shopOpened = !shopOpened;
}

bool Screen::isRunning() const{
    return window.isOpen();
}

void Screen::prepareDrawing(){
    window.clear(sf::Color::White);
}

void Screen::draw(Game& game){ 
    prepareDrawing();
    drawGame(game);
    endDrawing();
}
void Screen::adapteViews(unsigned int width, unsigned int height){
    sf::FloatRect rect(0, 0, width, height);
    view.reset(rect);
    interfaceView.reset(rect);        
}

void Screen::drawGame(Game& game){
    // Draw the land and the entities
    window.setView(view);
    centerView(game);
    drawBackLand(game.getLand());
    drawEntities(game);
    drawLand(game.getLand());
    
    drawLandForeGround(game.getLand());
    window.setView(interfaceView);
    drawShop(game);
    drawButtons(false);
    drawMenuBar();
    drawCarac(game);
    drawChat(game);
    drawButtons(true);
}

void Screen::drawMenuBar(){
    window.draw(createSprite("barre-menu", sf::Vector2f(0, 0),window.getView().getSize().x));
}

void Screen::drawShop(Game&){
    // If the shop is opened but its height is < shopMaxHeight
    if(shopOpened && shopHeight < shopMaxHeight){
        shopHeight = static_cast<unsigned int>(std::min(static_cast<int>(shopHeight + shopPanelSpeed), static_cast<int>(shopMaxHeight)));
    }
    // Else, if the shop is closed but its height is > 0
    else if(!shopOpened && shopHeight > 0){
        shopHeight = static_cast<unsigned int>(std::max(static_cast<int>(shopHeight - shopPanelSpeed), 0));
    }
    
    // Draw the shop
    window.draw(createSprite("barre-shop", sf::Vector2f(window.getView().getSize().x-300, static_cast<int>(40 + shopHeight - shopMaxHeight)), 300));
}

void Screen::drawButtons(bool aboveMenuBar){
    // Draw the buttons
    for(std::shared_ptr<Button>& button : buttons){
        // If the button should be drawn
        if(button->getAboveMenuBar() == aboveMenuBar){
            // If the button is pressed
            if(button->getPressed()){
                window.draw(createSprite("buttonPressed", button->getPosition(), button->getSize().x));
            }
            // Else
            else{
                window.draw(createSprite("buttonReleased", button->getPosition(), button->getSize().x));
            }
            
            // Draw the button text
            sf::Text text;
		    text.setFont(fontManager.filenameToFont("police"));
		    text.setString(button->getText());
		    text.setCharacterSize(button->getSize().y-10);
		    text.setPosition(button->getPosition() + sf::Vector2f(7, 5));
		    window.draw(text);
		}
    }
}

void Screen::drawChat(Game& game){
    // Draw the background
    sf::RectangleShape rectangle(sf::Vector2f(450, game.getMessages().size()*20 + 40));
    rectangle.setPosition(sf::Vector2f(10, window.getView().getSize().y - game.getMessages().size()*20 - 50));
    rectangle.setFillColor(sf::Color(0, 0, 0, 140));
    window.draw(rectangle);

    // Draw the messages
    int index = 0;
    for(const sf::String& message : game.getMessages()){
		// Draw the message
		sf::Text text;
		text.setFont(fontManager.filenameToFont("police"));
		text.setString(message);
		text.setCharacterSize(15);
		text.setPosition(20,window.getView().getSize().y-60-index*20);
		window.draw(text);
		
		// Increment index
		index++;    
	}
	
	// If we're writing a message
	if(game.getWritingMessage()){
	    // Draw the writing message bar
        rectangle = sf::RectangleShape(sf::Vector2f(442, 22));
        rectangle.setPosition(sf::Vector2f(14, window.getView().getSize().y - 38));
        rectangle.setFillColor(sf::Color(0, 0, 0, 100));
        window.draw(rectangle);
    }
	
	// Draw the message being written
	sf::Text text;
	text.setFont(fontManager.filenameToFont("police"));
	text.setString(game.getMessageBeingWritten());
	text.setCharacterSize(15);
	text.setPosition(20,window.getView().getSize().y-35);
	window.draw(text);
}

void Screen::drawCarac(Game& game){ 
    // If we have a player knight
    if(game.getPlayerKnight()){
        // Draw our hearts
        int heartQuantity = 0;
        int i;
        for(i = 0; i < game.getPlayerKnight()->getHp(); i++){
            heartQuantity += 1;
            if(heartQuantity == 4){
                window.draw(createSprite("heart4", sf::Vector2f(((i/4)*20)+10, 10)));
                heartQuantity = 0;
            }
        }
        if(heartQuantity > 0) window.draw(createSprite("heart" + std::to_string(heartQuantity), sf::Vector2f((((i-1)/4)*20)+10, 10)));
        
        // Draw the number of players
        sf::Text text;
		text.setFont(fontManager.filenameToFont("police"));
		text.setString(std::to_string(game.getKnights().size()) + " Player(s)" + "  Money : "+ std::to_string(game.getPlayerKnight()->getMoney()) );
		text.setCharacterSize(24);
		text.setPosition(((game.getPlayerKnight()->getHp() - 1)/4)*20 + 30, 5);
		window.draw(text);
    }
}
void Screen::centerView(Game& game){
    // If we have a player knight
    if(game.getPlayerKnight()){
        sf::Vector2f viewCenter;

        // On X
        if(game.getPlayerKnight()->getPosition().x-(window.getView().getSize().x/2) < 0){
            viewCenter.x = window.getView().getSize().x/2;
        }
        else if(game.getPlayerKnight()->getPosition().x+(window.getView().getSize().x/2) > game.getLand().getMapWidth()*Tile::size ){
            viewCenter.x = game.getLand().getMapWidth()*Tile::size - window.getView().getSize().x/2;
        }
        else{
            viewCenter.x = game.getPlayerKnight()->getPosition().x;
        }

        // On Y
        if(game.getPlayerKnight()->getPosition().y-(window.getView().getSize().y/2) < -sizeMenuBar){
            viewCenter.y = window.getView().getSize().y/2 - sizeMenuBar;
        }
        else if(game.getPlayerKnight()->getPosition().y+(window.getView().getSize().y/2) > game.getLand().getMapHeight()*Tile::size ){
            viewCenter.y = game.getLand().getMapHeight()*Tile::size - window.getView().getSize().y/2;
        }
        else{
            viewCenter.y = game.getPlayerKnight()->getPosition().y;
        }


        //viewCenter.y = game.getPlayerKnight()->getPosition().y;

        view.setCenter(viewCenter);
    }
}

void Screen::drawLand(Land& land){
    // Iterate over the tiles
    for(unsigned int i = 0 ; i < land.getMap().size() ; i++){
        for(unsigned int j = 0; j < land.getMap()[i].size(); j++){
        	if(!land.getMap()[i][j].getForeGround()){
		        switch(land.getMap()[i][j].getTileType()){
		           case TileType::empty:
		                //window.draw(createSprite("empty-land", sf::Vector2f(i * Tile::size, j * Tile::size)));				
		            break;
		           
		            case TileType::ground:
		                window.draw(createSprite("ground-land", sf::Vector2f(i * Tile::size, j * Tile::size)));	
		            break;
		            case TileType::castleRock:
		                window.draw(createSprite("castelrock-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalGround:
		                window.draw(createSprite("vegetalGround", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalCastleRock:
		                window.draw(createSprite("vegetalCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::spawn:
		                window.draw(createSprite("spawn", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::glass:
		              	window.draw(createSprite("glass", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::alternateCastleRock:
		             	window.draw(createSprite("alternateCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backCastleRock:
		             	//window.draw(createSprite("backCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backGround:
		             	//window.draw(createSprite("backGround-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		            case TileType::cloud:
		            	window.draw(createSprite("cloud", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		        }
		    }
        }
    }
}



void Screen::drawBackLand(Land& land){
    // Iterate over the tiles
    for(unsigned int i = 0 ; i < land.getMap().size() ; i++){
        for(unsigned int j = 0; j < land.getMap()[i].size(); j++){
        	if(!land.getMap()[i][j].getForeGround()){
		        switch(land.getMap()[i][j].getTileType()){
		            case TileType::empty:
		                window.draw(createSprite("empty-land", sf::Vector2f(i * Tile::size, j * Tile::size)));				
		            break;
		           
		            case TileType::ground:
		                //window.draw(createSprite("ground-land", sf::Vector2f(i * Tile::size, j * Tile::size)));	
		            break;
		            case TileType::castleRock:
		                //window.draw(createSprite("castelrock-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalGround:
		                //window.draw(createSprite("vegetalGround", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalCastleRock:
		                //window.draw(createSprite("vegetalCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::spawn:
		                //window.draw(createSprite("spawn", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::glass:
		              	//window.draw(createSprite("glass", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::alternateCastleRock:
		             	//window.draw(createSprite("alternateCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backCastleRock:
		             	window.draw(createSprite("backCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backGround:
		             	window.draw(createSprite("backGround-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		            case TileType::cloud:
		            	//window.draw(createSprite("cloud", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		            
		        }
		    }
        }
    }
}

void Screen::drawLandForeGround(Land& land){

 for(unsigned int i = 0 ; i < land.getMap().size() ; i++){
        for(unsigned int j = 0; j < land.getMap()[i].size(); j++){
        	if(land.getMap()[i][j].getForeGround()){
		        switch(land.getMap()[i][j].getTileType()){
					case TileType::empty:
		               // window.draw(createSprite("empty-land", sf::Vector2f(i * Tile::size, j * Tile::size)));				
		            break;
		            case TileType::ground:
		                window.draw(createSprite("ground-land", sf::Vector2f(i * Tile::size, j * Tile::size)));	
		            break;
		            case TileType::castleRock:
		                window.draw(createSprite("castelrock-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalGround:
		                window.draw(createSprite("vegetalGround", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::vegetalCastleRock:
		                window.draw(createSprite("vegetalCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::spawn:
		               // window.draw(createSprite("spawn", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		              case TileType::glass:
		              	window.draw(createSprite("glass", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::alternateCastleRock:
		             	window.draw(createSprite("alternateCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backCastleRock:
		             	//window.draw(createSprite("backCastleRock", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::backGround:
		             	//window.draw(createSprite("backGround-land", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		             case TileType::cloud:
		            	//window.draw(createSprite("cloud", sf::Vector2f(i * Tile::size, j * Tile::size)));
		            break;
		            
		        }
		    }
        }
    }

}

void Screen::drawEntities(Game& game){

	
    // Iterate over the knights except the PlayerKnight
    for(std::pair<sf::IpAddress, std::shared_ptr<Knight> > knight : game.getKnights()){
        // Draw the knight
        if (sf::IpAddress::getLocalAddress() != knight.first){
            drawKnight(*knight.second);         
        }
        
  

        
		
	
		
    }
    
    // If there is a player knight
    if(game.getPlayerKnight()){
    	drawKnight(*game.getPlayerKnight());
    	
    }
}

void Screen::drawKnight(Knight& knight){
    // Set the spine animation color factor
    knight.getSpineAnimation().setColorFactor(sf::Color(255, 255 - knight.getHurtColorTimer() * 15, 255 - knight.getHurtColorTimer() * 15));
    
    // Draw the spine animation
    window.draw(*(knight.getSpineAnimation().getDrawable()));
    
    // Draw the Pseudo !
    
    sf::Text text;
	text.setFont(fontManager.filenameToFont("police"));
	text.setString(knight.getPseudo()+" ("+std::to_string(static_cast<int>(std::ceil(static_cast<float>(knight.getHp())/4)))+" HP)");
	
	if(knight.getKing()){
		text.setCharacterSize(25);
		text.setColor(sf::Color(222, 193, 0));
		text.setStyle(sf::Text::Italic);
	}
	else {
		text.setCharacterSize(18);
	}
	
	text.setPosition(knight.getPosition().x + 45 - text.getGlobalBounds().width/2,knight.getPosition().y - (knight.getKing()? 120 : 100));
	window.draw(text);
}

sf::Sprite Screen::createSprite(std::string filename, sf::Vector2f position,float scaleX ,float scaleY){
	sf::Sprite sprite;
	sprite.setTexture(imageManager.filenameToTexture(filename));
    sprite.setPosition(position);
    sprite.setScale(scaleX,scaleY);
    
	return sprite;
}
sf::Sprite Screen::createFlipSprite(std::string filename, sf::Vector2f position){
	sf::Sprite sprite;
	sprite.setTexture(imageManager.filenameToTexture(filename));
    sprite.setPosition(position);
    sprite.setScale(-1,1);
	return sprite;
}

void Screen::endDrawing(){
    window.display();
}

bool Screen::pollEvent(sf::Event& event){
    return window.pollEvent(event);
}

void Screen::closeWindow(){
    window.close();
}

