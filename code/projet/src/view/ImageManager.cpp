#include "ImageManager.hpp"

ImageManager::ImageManager(){
	

}

sf::Texture& ImageManager::filenameToTexture(std::string h){
    // Add the folder name
    h = "./images/" + h +".png";
    
    // If the texture isn't loaded yet, load it
	if(map.count(h) == 0){
		ImageManager::addTexture(h);
    }
    
    // Return the texture
    return map[h];
}

void ImageManager::addTexture(std::string h){
    // Create the texture
	map[h] = sf::Texture();
    
    // Try to load it
	if(!map[h].loadFromFile(h)){
		std::cout << "Error when loading the image " << h << "." << std::endl;
	}
}
