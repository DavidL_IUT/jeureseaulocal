#ifndef HPP_IMAGEMANAGER
#define HPP_IMAGEMANAGER
#include <map>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
class ImageManager{
    public:
        ImageManager();
	
	sf::Texture& filenameToTexture(std::string h);
        
    private:
	std::map<std::string,sf::Texture> map;
	void addTexture(std::string h);
	


	
	
};

#endif
