#ifndef HPP_BUTTONOPENSHOP
#define HPP_BUTTONOPENSHOP

#include "Button.hpp"

class Screen;

class ButtonOpenShop : public Button{
    public:
        ButtonOpenShop(bool, sf::Vector2f, sf::Vector2f, std::string, Screen&);
    
        // Perform action
        void performAction();
        
        // Get the button position
        virtual sf::Vector2f getPosition() const;
    
    private:
        // The screen
        Screen& screen;
};

#endif
