#include "SpineAnimation.hpp"

SpineAnimation::SpineAnimation(std::string _name) :
    name(_name){
    // Load atlas and skeleton data
    
    atlas = Atlas_createFromFile(("spine/" + name + ".atlas").c_str(), 0);
    SkeletonJson* json = SkeletonJson_create(atlas);
    json->scale = 0.5f;
    skeletonData = SkeletonJson_readSkeletonDataFile(json, ("spine/" + name + ".json").c_str());
    if(!skeletonData) {
        std::cout << "Error while loading a spine skeleton data : " << json->error << std::endl;
    }
    SkeletonJson_dispose(json);

    // Load drawable
    skeletonDrawable = new spine::SkeletonDrawable(skeletonData); // TODO : delete ?
    skeletonDrawable->timeScale = 1;
}

SpineAnimation::~SpineAnimation(){
    SkeletonData_dispose(skeletonData);
    Atlas_dispose(atlas);
}

void SpineAnimation::loop(float delta){
    // Update the drawable
    skeletonDrawable->update(delta);
}

void SpineAnimation::setPosition(sf::Vector2f position){
    skeletonDrawable->skeleton->x = position.x + 45;
    skeletonDrawable->skeleton->y = position.y + 192.82;
    Skeleton_updateWorldTransform(skeletonDrawable->skeleton);
}

void SpineAnimation::setAnimation(std::string animationName, bool looping, bool flip){
    // If...
	if(spAnimationState_getCurrent(getDrawable()->state, 0) == NULL || // ...we were not playing any animation
       animationName != std::string(spAnimationState_getCurrent(getDrawable()->state, 0)->animation->name) || // or this one has a different name
       skeletonDrawable->skeleton->flipX != flip){ // or this one has a different flip boolean
        // Launch the new animation
		skeletonDrawable->skeleton->flipX = flip;
		Skeleton_updateWorldTransform(skeletonDrawable->skeleton);
		AnimationState_setAnimationByName(skeletonDrawable->state, 0, animationName.c_str(), looping); // ATTENTION PROBLEME DE SIGSEGV, Segmentation fault.
	}
}


void SpineAnimation::setSkin(std::string skinName){
	Skeleton_setSkin(skeletonDrawable->skeleton ,SkeletonData_findSkin(getSkeletonData() , skinName.c_str()));
	//Skeleton_setSlotsToSetupPose (skeletonDrawable->skeleton);
	Skeleton_updateWorldTransform(skeletonDrawable->skeleton);
}

void SpineAnimation::loadSlot(std::string slotName){
 Slot_setToSetupPose(Skeleton_findSlot(skeletonDrawable->skeleton, slotName.c_str()));
}

void SpineAnimation::setPartSkin(std::string part, std::string skinName){
	Skeleton_setAttachment(skeletonDrawable->skeleton
	, part.c_str()
	, skinName.c_str());
	Skeleton_updateWorldTransform(skeletonDrawable->skeleton);
}





void SpineAnimation::setColorFactor(sf::Color colorFactor){
    skeletonDrawable->colorFactor = colorFactor;
}
