#include "Button.hpp"

#include "Screen.hpp"

Button::Button(bool _aboveMenuBar, sf::Vector2f _position, sf::Vector2f _size, std::string _text) :
    aboveMenuBar(_aboveMenuBar),
    position(_position),
    size(_size),
    text(_text){
    // At first, the button isn't pressed
    pressed = false;
}

void Button::onEvent(Screen& screen, sf::Event event){
    switch(event.type){
        case sf::Event::MouseButtonPressed:
            // Depending on the mouse button
            switch(event.mouseButton.button){
                case sf::Mouse::Left:
                    // If the mouse is inside the button
                    if(isMouseInsideButton(screen)){
                        // The button is pressed
                        pressed = true;
                    }
                break;
                default: break;
            }
        break;
        case sf::Event::MouseButtonReleased:
            // Depending on the mouse button
            switch(event.mouseButton.button){
                case sf::Mouse::Left:
                    // The button isn't pressed anymore
                    pressed = false;
                    
                    // If the mouse was inside the button, call the performAction method
                    if(isMouseInsideButton(screen)){
                       performAction();
                    }
                break;
                default: break;
            }
        break;
        default: break;
    }
}

bool Button::isMouseInsideButton(Screen& screen){
    // If the mouse is outside the button, return false
    if(sf::Mouse::getPosition(screen.getWindow()).x < getPosition().x)
        return false;
        
    if(sf::Mouse::getPosition(screen.getWindow()).x > getPosition().x + size.x)
        return false;
        
    if(sf::Mouse::getPosition(screen.getWindow()).y < getPosition().y)
        return false;
        
    if(sf::Mouse::getPosition(screen.getWindow()).y > getPosition().y + size.y)
        return false;
        
    // Else, the mouse is inside the button, return true
    return true;
}
