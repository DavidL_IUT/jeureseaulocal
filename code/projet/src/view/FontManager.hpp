#ifndef HPP_FONTMANAGER
#define HPP_FONTMANAGER
#include <map>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
class FontManager{
    public:
        FontManager();
        
        // Filename to font
        sf::Font& filenameToFont(std::string h);
            
    private:
        // String to font map
        std::map<std::string, sf::Font> map;
        
        // Add a font
        void addFont(std::string h);
};

#endif
