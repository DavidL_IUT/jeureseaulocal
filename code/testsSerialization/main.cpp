#include <list>
#include <iostream>

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

// Structure Data
struct Data{
    sf::Int32 entier1;
    sf::Vector2f position;
    std::list<std::string> messages;
    sf::Int32 entier2;
};

// Transfert de Data vers un packet
sf::Packet& operator <<(sf::Packet& packet, const sf::Vector2f& vector2f){
    return packet << vector2f.x << vector2f.y;
}

sf::Packet& operator <<(sf::Packet& packet, const std::list<std::string>& stringList){
    packet << static_cast<sf::Uint32>(stringList.size());
    
    for(const std::string& string : stringList){
        packet << string;
    }
    
    return packet;
}

sf::Packet& operator <<(sf::Packet& packet, const Data& data){
    return packet << data.entier1 << data.position << data.messages << data.entier2;
}

// Transfert d'un packet vers Data
sf::Packet& operator >>(sf::Packet& packet, sf::Vector2f& vector2f){
    return packet >> vector2f.x >> vector2f.y;
}

sf::Packet& operator >>(sf::Packet& packet, std::list<std::string>& stringList){
    sf::Uint32 stringListSize;
    packet >> stringListSize;
    
    for(unsigned int i = 0; i < stringListSize; i++){
        std::string string;
        packet >> string;
        stringList.push_back(string);
    }
    
    return packet;
}

sf::Packet& operator >>(sf::Packet& packet, Data& data){
    return packet >> data.entier1 >> data.position >> data.messages >> data.entier2;
}

int main(){
    // On crée data1
    Data data1;
    data1.entier1 = 10;
    data1.position = sf::Vector2f(20, 30);
    data1.messages.push_back("test");
    data1.messages.push_back("ouaiiiis");
    data1.entier2 = 40;
    
    // On crée le packet
    sf::Packet packet;
    
    // On met data1 dans le packet
    packet << data1;
    
    // On crée data2
    Data data2;
    
    // On met le packet dans data2
    packet >> data2;
    
    // On affiche le contenu de data2
    std::cout << data2.entier1 << ", (" << data2.position.x << ", " << data2.position.y << "), "
    << data2.messages.front() << ", " << data2.messages.back() << ", " << data2.entier2 << std::endl;
    
    return 0;
}
