#ifndef HPP_CLIENT
#define HPP_CLIENT

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <list> 
#include <iostream>
class Client{
	public:
    	Client();
        
    	bool isServer();
    	void loop();
    	

	private:
        // Static variables
		static std::list<unsigned short> possiblePorts;
		static std::string broadcastMessage;
		static std::string serverBroadcastResponse;
		static sf::Time timing;
        
        // Socket, port, and connected clients by priority order (including ourselves)
        sf::UdpSocket socket;
    	unsigned short port; // Port on which the socket is bound
    	std::list<sf::IpAddress> connectedClients;
    	std::map<sf::IpAddress,sf::Clock> timerClients;
        
        
        // Test if a given ip address is known
        bool isKnown(sf::IpAddress);
        
        // Broadcast-related methods
        bool bindOnPort(unsigned short); // Return true if we managed to bind on the given port
        bool findAServer(); // Return true if a server was found on one of the possible ports
    	bool broadcastOnCurrentPort(); // Return true if a server was found on the current port
        bool bindOnOneOfPossiblePorts(); // Return true if we managed to bind on one of the possible ports
        bool waitForServerResponseToABroadcast(); // Return true if a server responded
        
        // Server-specific methods
    	void serverLoop();
        void serverReceivedPacketFromKnownClient(sf::Packet, sf::IpAddress);
        void serverReceivedPacketFromUnknownClient(sf::Packet, sf::IpAddress);
        void clearConnexion();
        
        // Client loop
    	void clientLoop();
};
#endif
