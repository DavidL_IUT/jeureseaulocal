#include "Network.hpp"


bool Network::broadcast_init(sf::UdpSocket *socket , sf::IpAddress serveur){
	char data[100]= {'a'}; // TEST ! (envoie d'un message)
	if (socket->send(data, 100, sf::IpAddress::Broadcast, this->port) != sf::Socket::Done){ // L'ipAdress::Broadcast permet de faire un broadcast sur le reseau !
 	   printf("Erreur Broadcast ! \n");
	}
	std::cout << "... Broadcast done ...\n";
	
	sf::IpAddress me = sf::IpAddress::getLocalAddress(); // Mon adresse local !
	sf::IpAddress sender; // Définition de l'adresse de l'"envoyeur"
	std::size_t received; // Taille du contenu receptionné
	
	 /* @TODO : Il faut faire en sorte que si on reçoit une réponse à notre broadcast 
	 par un hote différent que nous même , envoyé des messages en boucle sur cette hote,
	 sinon se mettre en mode "serveur" */
	socket->setBlocking(false); // On ne veux pas forcément attendre un message, on passe la socket en mode : Non bloquante.
	bool server_on = false; // désigne s'il y a un serveur sur le réseau local
	for (int i =0 ; i <2 ; i++){
	
	if (socket->receive(data, 100, received, sender, this->port) != sf::Socket::Done){ 
	    // erreur...
	}
	if ((me.toInteger() != sender.toInteger()) && (sender.toInteger() != 0)){ // Si on me répond et que je ne suis pas l'envoyeur ! (Convertit les adresses en entiers pour la comparaison)

	std::cout << "Received " << received << " bytes from " << sender << " on port " << port << std::endl;
	server_on = true;
	}
	else {
	std::cout << "Je reçoit quelque chose de moi même ..." << std::endl;
	}
	sleep(1); // Temps d'attente pour laisser le temps au serveur de répondre dans le cas où il est présent.
	}
	socket->setBlocking(true);
	return (server_on);
      
 }

Network::Network(int p){ //Initilisation 
	this->port = (unsigned short) p; // Port declaration
	sf::UdpSocket socket; // Création d'une sockette, on ne peux la mettre dans la classe ! (sauf sous forme de pointeur)
	socket.bind(p); // Bind de la sockette 
	sf::IpAddress serveur; // Définit l'adresse IP du serveur , dans le cas où elle est = à la notre sur le réseau alors on est le serveur !
	/* Pour regarder si on est le serveur il suffit donc de regarder si serveur == f::IpAddress::getLocalAddress(); */
	char data[100];
	sf::IpAddress sender; // Définition de l'adresse de l'"envoyeur"
	std::size_t received; // Taille du contenu receptionné
	if(Network::broadcast_init(&socket , serveur)){ //Broadcast sur le reseau pour voir si on a bien un serveur d'actif.
		std::cout <<"Mode client : on \n";
		while(1){

			for(int i =0 ; i<15 ; i++){
				if (socket.send(data, 100, serveur, this->port) != sf::Socket::Done){ // L'ipAdress::Broadcast permet de faire un broadcast sur le reseau !
		 	   printf("Erreur envoie ! \n");
				}
			}
			sleep(1);
		}
	} else 
	{
		std::cout <<"Mode Serveur : on \n";
	while(1){
		
	if (socket.receive(data, 100, received, sender, this->port) != sf::Socket::Done){ 
	    // erreur...
	}
	std::cout << "Received " << received << " bytes from " << sender << " on port " << port << std::endl;
	/** Renvoie directe du message reçut ! , celà fonctionne sur une machine virtuelle avec une adresse IP différentes , il reste néanmoins
	d'autre test à faire dans un environnement "normal" , et à configuré le client mieux que je ne peux le faire dans les conditions actuelles ! **/
	
	if (socket.send(data, 100, sender, this->port) != sf::Socket::Done){ // L'ipAdress::Broadcast permet de faire un broadcast sur le reseau !
		 	   printf("Erreur envoie ! \n");
	}
	}
	}
}



int main(int argc, char* argv[]){
	Network n = Network(25500); // Création du "Serveur" avec le port (int)


}
