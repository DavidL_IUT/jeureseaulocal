#ifndef HPP_DATA
#define HPP_DATA

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <list> 
#include <iostream>
class Data{
	public:
    	Data(sf::Int32,sf::Vector2f,std::list<std::string>,sf::Int32);
        
	private:
        sf::Int32 entier1;
		sf::Vector2f position;
		std::list<std::string> messages;
		sf::Int32 entier2;
};

#endif
