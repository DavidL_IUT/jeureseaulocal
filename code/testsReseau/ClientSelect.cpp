#include "ClientSelect.hpp"

const std::list<unsigned short> ClientSelect::possiblePorts = {25500, 26504, 35109, 31027};
const std::string ClientSelect::broadcastMessage = "YatilUnServer?";
const std::string ClientSelect::serverBroadcastResponse = "UnServeurExisteDeja!";;
const int ClientSelect::sendingAttemptNumber = 3;
const float ClientSelect::timeToWaitForServerAnswer = 1;
const float ClientSelect::timeToWaitForClientMessage = 0.5;
const float ClientSelect::timeToWaitForServerData = 0.5;

ClientSelect::ClientSelect(){
    // Set the socket on non-blocking mode
	socket.setBlocking(false);
    
    // If we manage to bind on a possible port
    if(bindOnOneOfPossiblePorts()){
        std::cout << "Socket bound on port " << port << "." << std::endl;
        // If no server was found on all possible ports
        if(!findAServer()){
            std::cout << "No server found." << std::endl;
            std::cout << "We become the server on port " << port << "." << std::endl;
            // We will be the server!!
            connectedClients.push_front(ClientPair(sf::IpAddress::getLocalAddress(), port));
        }
        // Else, we found a server
        else{
            std::cout << "Found a server at " << serverIp << ":" << serverPort << "." << std::endl;
        }
    }
    // Else, we didn't found any open port
    else{
        std::cout << "No open port found :(" << std::endl;
    }
}

sf::Socket::Status ClientSelect::bindOnPort(unsigned short newPort){
    // Bind on the port and keep the status
    sf::Socket::Status status = socket.bind(newPort);
    
    // If we manage to bind on this new port, set the port
    if(status == sf::Socket::Status::Done){
        // Add the socket to the selector
        selector.add(socket);
        
        // Set the port
        port = newPort;
    }
    
    // Return the status
    return status;
}

bool ClientSelect::bindOnOneOfPossiblePorts(){
    std::cout << "Trying to bind the socket on one of the possible ports..." << std::endl;
    
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        // If we manage to bind on the possible port
        if(bindOnPort(possiblePort) == sf::Socket::Status::Done){            
            // Return true
            return true;
        }
    }
    
    // We didn't manage to bind on a port, return false
    return false;
}

bool ClientSelect::findAServer(){
    std::cout << "Trying to find a server..." << std::endl;

    // Broadcast on possible ports
    broadcastOnPossiblePorts();

    // We now wait for an response from a server
    if(waitForServerResponseToABroadcast()){
        return true;
    }
    
    // No server was found, return false
    return false;
}


void ClientSelect::broadcastOnPossiblePorts(){
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        std::cout << "Trying to send a broadcast on port " << possiblePort << "..." << std::endl;
        
        // Create the packet and put the broadcast message in it
        sf::Packet packet;
        packet << broadcastMessage;
        
        // Send the broadcast
        if(multipleSend(packet, sf::IpAddress::Broadcast, possiblePort) == sf::Socket::Done){
            std::cout << "Broadcast sent successfully." << std::endl;
        }
        else{
            std::cout << "Failed to send the broadcast on port" << possiblePort << "." << std::endl;
        }
    }
}

sf::Socket::Status ClientSelect::multipleSend(sf::Packet packet, sf::IpAddress ipAddress, unsigned short sendPort){
    // Create the status we will return in the end
    sf::Socket::Status status = sf::Socket::Status::Error;
    
    // Send
    for(int i = 0; i < sendingAttemptNumber; i++){
        if(socket.send(packet, ipAddress, sendPort) == sf::Socket::Status::Done){
            status = sf::Socket::Status::Done;
        }
    }
    
    // Return the status
    return status;
}

bool ClientSelect::waitForServerResponseToABroadcast(){
    std::cout << "Waiting for a server response to the broadcast..." << std::endl;
    
    // Create the timer
    sf::Clock timer;
    timer.restart();
    
    // Create the packet, server address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForServerAnswer) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is not us
            if(senderAddress != sf::IpAddress::getLocalAddress()){
                // Find and show the received message
                std::string receivedMessage;
                packet >> receivedMessage;
                std::cout << "Received message " << receivedMessage << "." << std::endl;
                
                // If the received message is the server broadcast response, then there is a server, we return true
                if(receivedMessage == serverBroadcastResponse){
                    // Else, if the sender of the boradcast reponse isn't known
                    if(!isKnown(ClientPair(senderAddress, senderPort))){
                        serverIp = senderAddress;
                        serverPort = senderPort;
                        return true;
                    }
                    // Else
                    else{
                        std::cout << "Concurrency problem : finding the lowest IP address..." << std::endl;
                        
                        // Find the smallest IP address between ourselves and our added connected clients
                        sf::Uint32 smallestIpNumber = sf::IpAddress::getLocalAddress().toInteger();
                        sf::IpAddress smallestIp = sf::IpAddress::getLocalAddress();
                        for(ClientPair client : connectedClients){
                            if(client.first.toInteger() < smallestIpNumber){
                                smallestIpNumber = client.first.toInteger();
                                smallestIp = client.first;
                            }
                        }
                        
                        std::cout << "Lowest IP address is " << smallestIp << "." << std::endl;
                        
                        // If we're not the smallest ip
                        if(smallestIp != sf::IpAddress::getLocalAddress()){
                            std::cout << "We're not the smallest ip, we won't be the server after all." << std::endl;
                            
                            // Return true, we won't be the server after all
                            serverIp = senderAddress;
                            serverPort = senderPort;
                            return true;
                        }
                        // Else
                        else{
                            std::cout << "We are the smallest IP, continue waiting for another server response to the broadcast..." << std::endl;
                        }
                    }
                }
                // Else, the received message is the broadcast message
                else if(receivedMessage == broadcastMessage){
                    answerClientBroadcastMessage(ClientPair(senderAddress, senderPort));
                }
            }
        }
    }

    // Return false
    return false;
}

bool ClientSelect::isServer(){
	return sf::IpAddress::getLocalAddress() == connectedClients.front().first;
}

bool ClientSelect::isKnown(ClientPair client){
	for(ClientPair& testedClient : connectedClients){
		if(client == testedClient){
			return true;
        }
	}
	return false;
}

void ClientSelect::loop(){
	if(isServer()){
		serverLoop();
	}
	else{
		clientLoop();	
	}
}

		//le serveur gère :
			//recevoir demande par broadcast
			//recevoir donnee client et traitement
			//envoyer les donnees a tous les clients 15fois/sec
void ClientSelect::serverLoop(){
    // Create the packet, sender address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
    
    // Create the timer
    sf::Clock timer;
    timer.restart();
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForClientMessage) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is not us
            if(senderAddress != sf::IpAddress::getLocalAddress()){
                std::cout << "The server received a packet from " << senderAddress << ":" << senderPort << std::endl;
                // If the address is known
                if(isKnown(ClientPair(senderAddress, senderPort))){
                    serverReceivedPacketFromKnownClient(packet, ClientPair(senderAddress, senderPort));
                }
                // Else
                else{
                    serverReceivedPacketFromUnknownClient(packet, ClientPair(senderAddress, senderPort));
                }
            }
        }
	}
    
    // Send data to all clients
    sendDataToAllClients();
	
	// Check if any deconnexion from client
	clearConnexion();
}

void ClientSelect::sendDataToAllClients(){
    // Create the packet
    sf::Packet packet;
    packet << 42;
    
    // Send the packet to all clients
	for(ClientPair client : connectedClients){
        if(multipleSend(packet, client.first, client.second) == sf::Socket::Done){
			
		}
	}
}

void ClientSelect::clearConnexion(){
	//@DOING
	
}

void ClientSelect::serverReceivedPacketFromKnownClient(sf::Packet packet, ClientPair client){
    int test;
    packet >> test;
    
    std::cout << "received from client : " << test << std::endl;
    
    // Restart the client timer
    clientsTimers[client].restart();
}

void ClientSelect::serverReceivedPacketFromUnknownClient(sf::Packet receivedPacket, ClientPair client){
    std::cout << "Received packet from unknown client." << std::endl;
    
    // Find the received message
    std::string receivedMessage;
    receivedPacket >> receivedMessage;

    // If the received message is the broadcast message
    if(receivedMessage == broadcastMessage){
        std::cout << "The unknown client sent the right broadcast message." << std::endl;
        
        answerClientBroadcastMessage(client);
    }
    else{
        std::cout << "The unknown client sent a wrong broadcast message!" << std::endl;
    }
}

void ClientSelect::answerClientBroadcastMessage(ClientPair client){
    // Prepare the packet to send to the unknown client
    sf::Packet packet;
    packet << serverBroadcastResponse;
    
    std::cout << "Sending the broadcast response to the unknown client..." << std::endl;
    // If we manage to send	
    if(multipleSend(packet, client.first, client.second) == sf::Socket::Done){
        std::cout << "New client connected from " << client.first << ":" << client.second << "." << std::endl;
        
        // Add the unknown client to the list and set its timer
        addNewConnectedClient(ClientPair(client.first, client.second));
    }
    // Else
    else{
        std::cout << "Failed sending a response to the unknown client." << std::endl;
    }
}

void ClientSelect::addNewConnectedClient(ClientPair client){
    // If the client isn't known yet
    if(!isKnown(client)){
        std::cout << "Adding new connected client " << client.first << ":" << client.second << "." << std::endl;
        
        // Add the client
        connectedClients.push_back(client);
        clientsTimers[client] = sf::Clock();
    }
    // Else
    else{
        std::cout << "NOT adding new connected client " << client.first << ":" << client.second << " because it is already known." << std::endl;
    }
}

void ClientSelect::clientLoop(){
	sendData();
    receiveServerData();
    
	//if not receive some data since a long time...DEAD
	
}

void ClientSelect::sendData(){
    // Create the packet
    sf::Packet packet;
    packet << 98;
    
    // Send the packet to the server
    multipleSend(packet, serverIp, serverPort);
}

void ClientSelect::receiveServerData(){
    // Create the packet, sender address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
	
    // Create the timer
    sf::Clock timer;
    
	// While we receive packets
	while(selector.wait(sf::seconds(timeToWaitForServerData) - timer.getElapsedTime())){
        while(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done){
            // If it is the server
            if(senderAddress == serverIp && senderPort == serverPort){
                int test;
                packet >> test;
                std::cout << "Réception de données du serveur : " << test << std::endl;
            }
        }
	}
}

