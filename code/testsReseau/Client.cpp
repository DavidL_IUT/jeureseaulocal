#include "Client.hpp"

std::list<unsigned short> Client::possiblePorts = {25500, 26504, 35109, 31027};
std::string Client::broadcastMessage = "YatilUnServer?";
std::string Client::serverBroadcastResponse = "UnServeurExisteDeja!";
sf::Time Client::timing = sf::seconds(6); //how many time the loop is run

Client::Client(){
    // Set the socket on non-blocking mode
	socket.setBlocking(false);
    
    // If no server was found on all possible ports
	if(!findAServer()){
        std::cout << "No server found." << std::endl;
        // If we manage to bind on a possible port
        if(bindOnOneOfPossiblePorts()){
            std::cout << "Socket bound on port " << port << "." << std::endl;
            std::cout << "Checking if there is still no server on port " << port << "." << std::endl;
            // If there is still no server on this port
            if(!broadcastOnCurrentPort()){
                std::cout << "We become the server on port " << port << "." << std::endl;
                // We will be the server!!
                connectedClients.push_back(sf::IpAddress::getLocalAddress());
            }
            // Else, now there's a server on this port
            else{
                std::cout << "There is now a server on port " << port << std::endl;
            }
        }
        // Else, we didn't found any open port
        else{
            std::cout << "No open port found :(" << std::endl;
        }
	}
    // Else, we found a server
    else{
        std::cout << "Found a server on port " << port << "." << std::endl;
    }
}

bool Client::bindOnPort(unsigned short newPort){
    // Bind on the port and keep the status
    sf::Socket::Status status = socket.bind(newPort);
    
    // If we manage to bind on this new port, set the port
    if(status == sf::Socket::Status::Done){
        port = newPort;
    }
    
    // Return the status
    return status;
}

bool Client::bindOnOneOfPossiblePorts(){
    std::cout << "Trying to bind the socket on one of the possible ports..." << std::endl;
    
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        // If we manage to bind on the possible port
        if(bindOnPort(possiblePort) == sf::Socket::Status::Done){
            // Return true
            return true;
        }
    }
    
    // We didn't manage to bind on a port, return false
    return false;
}

bool Client::findAServer(){
    std::cout << "Trying to find a server..." << std::endl;
    
    // Iterate over possible ports
    for(unsigned short possiblePort : possiblePorts){
        // Set the current port from the possible port and bind the socket
        bindOnPort(possiblePort);
        
        // If we find a server, return true
        if(broadcastOnCurrentPort()){
            return true;
        }
        // Else, unbind the socket
        else socket.unbind();
    }
    
    // No server was found, return false
    return false;
}


bool Client::broadcastOnCurrentPort(){
	std::cout << "Trying to send a broadcast on port " << port << "..." << std::endl;
	
	sf::Packet packet;
	packet << broadcastMessage;
	if(socket.send(packet, sf::IpAddress::Broadcast, port) == sf::Socket::Done){
        std::cout << "Broadcast sent successfully." << std::endl;
        // We now wait for an response from a server and return the result
        return waitForServerResponseToABroadcast();
	}
	else{
        std::cout << "Failed to send the broadcast." << std::endl;
		return false;
	}
}

bool Client::waitForServerResponseToABroadcast(){
    std::cout << "Waiting for a server response to the broadcast..." << std::endl;
    
    // Create the timer
    sf::Clock timer;
    timer.restart();
    
    // Create the packet, server address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress serverAddress;
    unsigned short serverPort;
    
    // While the time isn't elapsed
    while(timer.getElapsedTime().asSeconds() < timing.asSeconds()/2){
        // If we receive a packet
        if(socket.receive(packet, serverAddress, serverPort) == sf::Socket::Done){
            // Find and show the received message
            std::string receivedMessage;
            packet >> receivedMessage;
            std::cout << "Received message " << receivedMessage << "." << std::endl;
            
            // If the received message is the server broadcast response, then there is a server, we return true
            if(receivedMessage == serverBroadcastResponse){
                return true;
            }
        }
    }
    
    // Return false
    return false;
}

bool Client::isServer(){
	return sf::IpAddress::getLocalAddress() == connectedClients.front();
}

bool Client::isKnown(sf::IpAddress ip){
	for(sf::IpAddress& testedIp : connectedClients){
		if(ip == testedIp)
			return true;
	}
	return false;
}

void Client::loop(){
	if(isServer()){
		serverLoop();
	}
	else {
		clientLoop();	
	}
}

		//le serveur gère :
			//recevoir demande par broadcast
			//recevoir donnee client et traitement
			//envoyer les donnees a tous les clients 15fois/sec
void Client::serverLoop(){
    // Create the packet, server address and port to give to the receive method
    sf::Packet packet;
    sf::IpAddress senderAddress;
    unsigned short senderPort;
    sf::Clock t;
    t.restart();
    
    while (t.getElapsedTime().asSeconds() < timing.asSeconds()/3){ //try receive au moins 2 secondes
		// If we receive packets
		if(socket.receive(packet, senderAddress, senderPort) == sf::Socket::Done ){
			std::cout << "The server received a packet from " << senderAddress << std::endl;
			// If the address is known
			if(isKnown(senderAddress)){
				serverReceivedPacketFromKnownClient(packet, senderAddress);
			}
			// Else
			else {
				serverReceivedPacketFromUnknownClient(packet, senderAddress);
			}
		}
	}
	//send data to every clients
	//@DOING
	packet << "Data : Server to Clients...";
	for (sf::IpAddress ip:connectedClients){
		if (socket.send(packet, ip, port) == sf::Socket::Done){
			
		}
	}
	
	//check if any deconnexion from client
	clearConnexion();
	
}

void Client::clearConnexion(){
	//@DOING
	
}

void Client::serverReceivedPacketFromKnownClient(sf::Packet packet, sf::IpAddress senderAddress){
    //the sender is known, i'm receiving info about himself
    //@TODO
    
    //update timer client
    timerClients.find(senderAddress)->second.restart();
    
}

void Client::serverReceivedPacketFromUnknownClient(sf::Packet receivedPacket, sf::IpAddress senderAddress){
    std::cout << "Received packet from unknwon client." << std::endl;
    
    // Find the received message
    std::string receivedMessage;
    receivedPacket >> receivedMessage;

    
    // If the received message is the broadcast message
    if(receivedMessage == broadcastMessage){
        std::cout << "The unknown client sent the right broadcast message." << std::endl;
        
        // Prepare the packet to send to the unknown client
        sf::Packet packet;
        packet << serverBroadcastResponse;
        
        std::cout << "Sending a response to the unknown client..." << std::endl;
        // If we manage to send
        if(socket.send(packet, senderAddress, port) == sf::Socket::Done){
            std::cout << "New client connected from " << senderAddress <<":"<<port<< "." << std::endl;
            
            // Add the unknown client to the list
            connectedClients.push_back(senderAddress);
            //Start a timer about this client
            timerClients.find(senderAddress)->second.restart();
        }
        // Else
        else{
            std::cout << "Failed sending a response to the unknown client." << std::endl;
        }			
    }
    else{
        std::cout << "The unknown client sent a wrong broadcast message!" << std::endl;
    }
}

void Client::clientLoop(){
	//@DOING
	//send my data
	
	//receiv some data about other players
	
	//if not receive some data since a long time...DEAD
	
}
