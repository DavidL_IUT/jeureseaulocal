#ifndef HPP_CLIENTSELECT
#define HPP_CLIENTSELECT

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <list> 
#include <iostream>

typedef std::pair<sf::IpAddress, unsigned short> ClientPair;

class ClientSelect{
	public:
    	ClientSelect();
        
    	bool isServer();
    	void loop();
    	

	private:
        // Static const variables
		static const std::list<unsigned short> possiblePorts;
		static const std::string broadcastMessage;
		static const std::string serverBroadcastResponse;
        static const int sendingAttemptNumber;
        static const float timeToWaitForServerAnswer;
        static const float timeToWaitForClientMessage;
        static const float timeToWaitForServerData;
        
        // Socket, port, and connected clients by priority order (including ourselves)
        sf::UdpSocket socket;
    	unsigned short port; // Port on which the socket is bound
    	std::list<ClientPair> connectedClients;
    	
        // Selector
    	sf::SocketSelector selector;
        
        // Server-specific variables
        std::map<ClientPair, sf::Clock> clientsTimers;
        
        // Client-specific variables
        sf::IpAddress serverIp;
        unsigned short serverPort;
        
        // Test if a given ip address is known
        bool isKnown(ClientPair);
        
        // Send a packet multiple times
        sf::Socket::Status multipleSend(sf::Packet, sf::IpAddress, unsigned short);
        
        // Broadcast-related methods
        sf::Socket::Status bindOnPort(unsigned short); // Return true if we managed to bind on the given port
        bool findAServer(); // Return true if a server was found on one of the possible ports
    	void broadcastOnPossiblePorts(); // Return true if a server was found on the given port
        bool bindOnOneOfPossiblePorts(); // Return true if we managed to bind on one of the possible ports
        bool waitForServerResponseToABroadcast(); // Return true if a server responded
        void answerClientBroadcastMessage(ClientPair); // Send a serverBroadcastReponse message to a client and add it to connected clients
        
        // Server-specific methods
    	void serverLoop();
        void serverReceivedPacketFromKnownClient(sf::Packet, ClientPair);
        void serverReceivedPacketFromUnknownClient(sf::Packet, ClientPair);
        void addNewConnectedClient(ClientPair);
        void clearConnexion();
        void sendDataToAllClients();
        
        // Client-specific methods
    	void clientLoop();
        void sendData();
        void receiveServerData();
};
#endif

